//字符串函数扩展

//去除字符串首尾空格
String.prototype.trim = function () {
    return this.replace(/(^\s*)|(\s*$)/g, "");
}

//去除字符串中所有空格
String.prototype.sTrim = function () {
    return this.replace(/\s/g, "");
}

//将字符串转化为JSON对象
String.prototype.toJson = function () {
    return JSON.parse(this);
}

//---------------界线-----------------

//日期格式扩展

//日期格式化输出
Date.prototype.format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1,                 //月份
        "d+": this.getDate(),                    //日
        "h+": this.getHours(),                   //小时
        "m+": this.getMinutes(),                 //分
        "s+": this.getSeconds(),                 //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds()             //毫秒
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;
}

//---------------界线-----------------

//自定义工具对象
var Strive = function () {
};

//验证对象是否为空
Strive.prototype.notNull = function (obj) {
    if (obj != undefined && obj != null) {
        return true;
    }
    return false;
}

//验证字符串是否为空
Strive.prototype.notEmpty = function (str) {
    if (str != undefined && str != null && str.trim() != "") {
        return true;
    }
    return false;
}

//将对象数组objArr中的某个属性取出形成新的数组后返回
Strive.prototype.flatObjects = function (objArr, attr) {
    var attrs = [];
    if (objArr != undefined && attr != undefined) {
        for (var i = 0; i < objArr.length; i++) {
            attrs[i] = objArr[i][attr];
        }
    }
    return attrs;
}

//禁止页面刷新
Strive.prototype.invalidRefresh = function () {
    document.oncontextmenu = function () {
        event.returnValue = false;
    }
    window.onhelp = function () {
        return false;
    }
    method_ = function (e) {
        var ev = e || window.event;
        var obj = ev.target || ev.srcElement;
        var t = obj.type || obj.getAttribute('type');
        var vReadOnly = obj.getAttribute('readonly');
        var vEnabled = obj.getAttribute('enabled');
        vReadOnly = vReadOnly == null ? false : vReadOnly;
        vEnabled = vEnabled == null ? true : vEnabled;
        var flag1 = (ev.keyCode == 8 && (t == "password" || t == "text" || t == "textarea") && (vReadOnly == true || vEnabled != true)) ? true : false;
        var flag2 = (ev.keyCode == 8 && t != "password" && t != "text" && t != "textarea") ? true : false;
        var flag3 = (ev.altKey) && ((ev.keyCode == 37) || (ev.keyCode == 39)) ? true : false;
        if (flag1 || flag2 || flag3) {
            event.returnValue = false;
        }
    }
    document.onkeyup = method_;
    document.onkeypress = method_;
    document.onkeydown = method_;
}

//去除字符串中的html标签，是否包括img标签，incImg为true，则不过滤img标签
Strive.prototype.filterHtml = function (html, incImg) {
    if (strive.notEmpty(html)) {
        if (true == incImg) {
            html = html.replace(/<\/?[^>(img)]*>/g, '');
        } else {
            html = html.replace(/<\/?[^>]*>/g, '');
        }
        html = html.replace(/[|]*\n/, '')
        html = html.replace(/&nbsp;/ig, '');
        return html;
    }
    return "";
}

//js生成uuid
Strive.prototype.uuid = function () {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = "";
    var uuid = s.join("");
    return uuid;
}

//截取字符串前N个字符并返回
Strive.prototype.subNStr = function (str, n) {
    if (str.length > n) {
        return str.substring(0, n) + "...";
    } else {
        return str;
    }
}

//设置Cookie
Strive.prototype.setCookie = function (cName, cValue, expiredays) {
    var exdate = new Date()
    exdate.setDate(exdate.getDate() + expiredays)
    document.cookie = cName + "=" + escape(cValue) +
        ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString())
}

//获取Cookie
Strive.prototype.getCookie = function (cName) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(cName + "=")
        if (c_start != -1) {
            c_start = c_start + cName.length + 1
            c_end = document.cookie.indexOf(";", c_start)
            if (c_end == -1) c_end = document.cookie.length
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}

//email格式验证
Strive.prototype.isEmail = function (email) {
    if (strive.notEmpty(email)) {
        var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/;
        return reg.test(email);
    }
    return false;
}

//mobile number 格式验证
Strive.prototype.isMobile = function (mobile) {
    if (strive.notEmpty(mobile)) {
        var reg = /^[1][34578][0-9]{9}$/;
        return reg.test(mobile);
    }
    return false;
}