package com.fight.strive.app.property;

import lombok.Data;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 应用配置文件
 *
 * @author ZHOUXIANG
 */
@Data
@Component
@PropertySource(value = "classpath:application.yml", encoding = "UTF-8")
public class AppProperties {
}
