package com.fight.strive.sys.modules.security.handler;

import com.fight.strive.sys.modules.response.dto.ResponseStatusDto;
import com.fight.strive.sys.utils.AjaxUtils;
import com.fight.strive.sys.utils.JSONUtils;
import com.fight.strive.sys.utils.ResponseUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ZHOUXIANG
 */
@SuppressWarnings("unused")
public class StriveAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws ServletException, IOException {
        if (AjaxUtils.isAjaxRequest(request)) {
            ResponseStatusDto<String> responseStatusDto = ResponseStatusDto.success("登录成功");
            ResponseUtils.writeJson(response, JSONUtils.obj2Json(responseStatusDto));
        } else {
            super.onAuthenticationSuccess(request, response, authentication);
        }
    }

}
