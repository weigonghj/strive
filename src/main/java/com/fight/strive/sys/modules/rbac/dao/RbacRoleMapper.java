package com.fight.strive.sys.modules.rbac.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fight.strive.sys.modules.rbac.entity.RbacRoleEntity;

/**
 * @author ZHOUXIANG
 */
public interface RbacRoleMapper extends BaseMapper<RbacRoleEntity> {
}
