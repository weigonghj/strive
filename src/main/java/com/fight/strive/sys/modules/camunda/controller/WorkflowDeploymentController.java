package com.fight.strive.sys.modules.camunda.controller;

import com.fight.strive.sys.modules.camunda.dto.WorkflowDeploymentDto;
import com.fight.strive.sys.modules.camunda.service.WorkflowDeploymentService;
import com.fight.strive.sys.modules.request.annotation.RestPostMapping;
import com.fight.strive.sys.modules.request.dto.PageRequest;
import com.fight.strive.sys.modules.response.dto.PageData;
import com.fight.strive.sys.modules.response.dto.ResponseStatusDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author ZHOUXIANG
 */
@Controller
@RequestMapping("/admin/sys/workflow/deployment")
@Api("流程模型管理接口")
public class WorkflowDeploymentController {

    @Resource
    private WorkflowDeploymentService workflowDeploymentService;

    @ApiOperation("部署bpmn.xml流程文件")
    @RestPostMapping("/deploy")
    public ResponseStatusDto<Void> bpmnModelDeployFile(
            MultipartFile file, String name) {
        workflowDeploymentService.bpmnModelDeployFile(file, name);
        return ResponseStatusDto.success();
    }

    @ApiOperation("部署bpmn流程内容")
    @RestPostMapping("/deployText")
    public ResponseStatusDto<Void> bpmnModelDeployText(
            @RequestBody Map<String, String> map) {
        workflowDeploymentService.bpmnModelDeployText(
                map.get("text"), map.get("name"));
        return ResponseStatusDto.success();
    }

    @ApiOperation("分页获取流程部署列表")
    @RestPostMapping("/list")
    public ResponseStatusDto<PageData<WorkflowDeploymentDto>> listDeploymentByPage(
            @RequestBody PageRequest<WorkflowDeploymentDto> pageRequest) {
        return ResponseStatusDto.success(
                workflowDeploymentService.listDeploymentByPage(pageRequest)
        );
    }

    @ApiOperation("重新部署")
    @RestPostMapping("/redeploy")
    public ResponseStatusDto<Void> redeployResource(
            @RequestBody WorkflowDeploymentDto dto) {
        workflowDeploymentService.redeployResource(dto.getId());
        return ResponseStatusDto.success();
    }

    @ApiOperation("删除流程部署")
    @RestPostMapping("/delete")
    public ResponseStatusDto<Void> deleteDeployment(@RequestBody WorkflowDeploymentDto dto) {
        workflowDeploymentService.deleteDeploymet(dto.getId());
        return ResponseStatusDto.success();
    }
}
