package com.fight.strive.sys.modules.oauth2.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fight.strive.sys.enums.StriveConstantsEnum;
import com.fight.strive.sys.modules.exception.StriveException;
import com.fight.strive.sys.modules.mybatisplus.utils.MyBatisPlusUtils;
import com.fight.strive.sys.modules.oauth2.constants.Oauth2Constants;
import com.fight.strive.sys.modules.oauth2.dao.StriveClientDetailsMapper;
import com.fight.strive.sys.modules.oauth2.dto.ClientDetailsDto;
import com.fight.strive.sys.modules.oauth2.entity.ClientDetailsEntity;
import com.fight.strive.sys.modules.oauth2.service.StriveClientDetailsService;
import com.fight.strive.sys.modules.rbac.service.RbacUserService;
import com.fight.strive.sys.modules.request.dto.PageRequest;
import com.fight.strive.sys.modules.response.dto.PageData;
import com.fight.strive.sys.modules.web.component.SpringContextHolder;
import com.fight.strive.sys.utils.BeanUtils;
import com.fight.strive.sys.utils.CollectionUtils;
import com.fight.strive.sys.utils.DigestUtils;
import com.fight.strive.sys.utils.ObjectUtils;
import com.fight.strive.sys.utils.StringUtils;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

/**
 * Oauth2客户端信息服务类
 *
 * @author ZHOUXIANG
 */
@Service
public class StriveClientDetailsServiceImpl
        extends ServiceImpl<StriveClientDetailsMapper, ClientDetailsEntity>
        implements StriveClientDetailsService {

    @Resource
    private RbacUserService rbacUserService;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public ClientDetailsEntity getClientDetailsByClientId(String clientId) {
        ClientDetailsEntity entity = this.getRedisData(
                Oauth2Constants.CLIENT_OAUTH2_ID_KEY, clientId);
        if (ObjectUtils.isNull(entity)) {
            QueryWrapper<ClientDetailsEntity> queryWrapper =
                    new QueryWrapper<ClientDetailsEntity>()
                            .eq("client_id", clientId);
            entity = this.baseMapper.getClientByClientId(queryWrapper);
            if (ObjectUtils.notNull(entity)) {
                this.updateRedisData(entity);
            }
        }
        return entity;
    }

    @Override
    public void saveClientDetails(ClientDetailsEntity entity) {
        if (!this.saveOrUpdate(entity)) {
            throw new StriveException("保存或更新Oauth2客户端信息失败");
        }
        this.updateRedisData(entity);
    }

    @Override
    public void saveClientDetails(@Valid ClientDetailsDto dto) {
        // 添加的客户端ID不能与系统内置的客户端ID相同
        if (StringUtils.equalsIgnoreCase(dto.getClientId(),
                StriveConstantsEnum.STRIVE_APP_ID.value)) {
            throw new StriveException("该appId已存在");
        }
        // 保证只有在secret字段有值时更新，否则将secret置null，忽略更新
        if (StringUtils.isNotBlank(dto.getClientSecret())) {
            String digest = DigestUtils.md5Hex(dto.getClientSecret());
            dto.setClientSecret(digest);
        } else {
            dto.setClientSecret(null);
        }
        ClientDetailsEntity entity = new ClientDetailsEntity();
        BeanUtils.copyPropertiesIgnoreNull(dto, entity);
        try {
            if (!this.saveOrUpdate(entity)) {
                throw new StriveException("客户端信息保存失败");
            }
            // 更新缓存
            this.updateRedisData(entity);
        }catch (DuplicateKeyException e) {
            throw new StriveException("该appId已存在");
        }
    }

    @Override
    public PageData<ClientDetailsEntity> getClientDetailsList(
            PageRequest<ClientDetailsDto> pageRequest) {
        QueryWrapper<ClientDetailsEntity> queryWrapper = new QueryWrapper<>();
        MyBatisPlusUtils.buildQuery(queryWrapper, pageRequest.getCondition());
        Page<ClientDetailsEntity> page = pageRequest.buildMybatisPlusPage();
        IPage<ClientDetailsEntity> iPage = this.page(page, queryWrapper);
        return new PageData<>(iPage);
    }

    @Override
    public void deleteClientDetails(ClientDetailsDto dto) {
        if (ObjectUtils.isNull(dto.getId())) {
            throw new StriveException("要删除的ID不存在");
        }
        this.deleteRedisData(this.getClientById(dto.getId()));
        this.removeById(dto.getId());
    }

    @Override
    public void deleteBatchClientDetails(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            throw new StriveException("要删除的ID不存在");
        }
        // 删除缓存
        for (Long id : ids) {
            this.deleteRedisData(this.getClientById(id));
        }
        // 删除数据库
        this.removeByIds(ids);
    }

    @Override
    public void oauth2Logout() {
        SecurityContext ctx = SecurityContextHolder.getContext();
        Authentication auth = ctx.getAuthentication();
        if (ObjectUtils.notNull(auth)) {
            OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails)auth.getDetails();
            String token = details.getTokenValue();
            if (StringUtils.isNotBlank(token)) {
                TokenStore tokenStore = SpringContextHolder.getBean(TokenStore.class);
                OAuth2AccessToken oAuth2AccessToken = tokenStore.readAccessToken(token);
                if (ObjectUtils.notNull(oAuth2AccessToken)) {
                    tokenStore.removeAccessToken(oAuth2AccessToken);
                }
            }
        }
    }

    @Override
    public void oauth2LogoutByLoginName(String loginName) {
        if (StringUtils.isNotBlank(loginName)) {
            TokenStore tokenStore = SpringContextHolder.getBean(TokenStore.class);
            Collection<OAuth2AccessToken> tokens =
                    tokenStore.findTokensByClientIdAndUserName(
                            "strive_app_id_sysplatform", loginName);
            if (CollectionUtils.isNotEmpty(tokens)) {
                tokens.forEach(oAuth2AccessToken -> {
                    try {
                        tokenStore.removeAccessToken(oAuth2AccessToken);
                    } catch (Exception e) {
                        log.error("{} token 注销失败");
                    }
                });
            }
        }
    }

    @Override
    public ClientDetailsEntity getClientById(Long id) {
        ClientDetailsEntity entity = this.getRedisData(
                Oauth2Constants.OAUTH2_ID_KEY, id);
        if (ObjectUtils.isNull(entity)) {
            entity = this.getById(id);
            if (ObjectUtils.notNull(entity)) {
                this.updateRedisData(entity);
            }
        }
        return entity;
    }

    @Override
    public void updateRedisData(ClientDetailsEntity entity) {
        // oauth id -> entity
        redisTemplate.opsForHash().put(
                Oauth2Constants.OAUTH2_ID_KEY, entity.getId(), entity);
        // client id -> oauth id
        redisTemplate.opsForHash().put(
                Oauth2Constants.CLIENT_OAUTH2_ID_KEY,
                entity.getClientId(), entity.getId());
    }

    @Override
    public ClientDetailsEntity getRedisData(String key, Object hashKey) {
        if (Oauth2Constants.OAUTH2_ID_KEY.equals(key)) {
            return (ClientDetailsEntity) redisTemplate.opsForHash().get(
                    Oauth2Constants.OAUTH2_ID_KEY, hashKey);
        } else {
            Long id = (Long) redisTemplate.opsForHash().get(key, hashKey);
            if (ObjectUtils.notNull(id)) {
                return (ClientDetailsEntity) redisTemplate.opsForHash().get(
                        Oauth2Constants.OAUTH2_ID_KEY, id);
            }
        }
        return null;
    }

    @Override
    public void deleteRedisData(ClientDetailsEntity entity) {
        // oauth id -> entity
        redisTemplate.opsForHash().delete(
                Oauth2Constants.OAUTH2_ID_KEY, entity.getId());
        // client id -> oauth id
        redisTemplate.opsForHash().delete(
                Oauth2Constants.CLIENT_OAUTH2_ID_KEY,
                entity.getClientId());
    }
}
