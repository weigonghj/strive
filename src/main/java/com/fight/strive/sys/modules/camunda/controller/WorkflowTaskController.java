package com.fight.strive.sys.modules.camunda.controller;

import com.fight.strive.sys.modules.camunda.dto.WorkflowFormData;
import com.fight.strive.sys.modules.camunda.dto.WorkflowNodesDto;
import com.fight.strive.sys.modules.camunda.dto.WorkflowQueryDto;
import com.fight.strive.sys.modules.camunda.dto.WorkflowVariableDto;
import com.fight.strive.sys.modules.camunda.entity.WorkflowTaskEntity;
import com.fight.strive.sys.modules.camunda.service.WorkflowTaskService;
import com.fight.strive.sys.modules.request.annotation.RestGetMapping;
import com.fight.strive.sys.modules.request.annotation.RestPostMapping;
import com.fight.strive.sys.modules.request.dto.PageRequest;
import com.fight.strive.sys.modules.response.dto.PageData;
import com.fight.strive.sys.modules.response.dto.ResponseStatusDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * @author ZHOUXIANG
 */
@Controller
@RequestMapping("/admin/sys/workflow/task")
@Api("流程任务管理接口")
public class WorkflowTaskController {

    @Resource
    private WorkflowTaskService workflowTaskService;

    @ApiOperation("获取流程表单")
    @RestGetMapping("/getStartFormData/{id}")
    public ResponseStatusDto<WorkflowFormData> getStartFormData(
            @PathVariable String id) {
        return ResponseStatusDto.success(
                workflowTaskService.getStartFormData(id)
        );
    }

    @ApiOperation("获取任务表单")
    @RestGetMapping("/getTaskFormData/{id}")
    public ResponseStatusDto<WorkflowFormData> getTaskFormData(
            @PathVariable String id) {
        return ResponseStatusDto.success(
                workflowTaskService.getTaskFormData(id)
        );
    }

    @ApiOperation("发起流程")
    @RestPostMapping("/startProcess/{key}")
    public ResponseStatusDto<Void> startProcess(
            @PathVariable String key,
            @RequestBody WorkflowVariableDto dto) {
        workflowTaskService.startProcess(key, dto);
        return ResponseStatusDto.success();
    }

    @ApiOperation("获取流程实例")
    @RestPostMapping("/getMyProcessInstances")
    public ResponseStatusDto<PageData<HistoricProcessInstance>> getMyProcessInstances(
            @RequestBody PageRequest<WorkflowQueryDto> pageRequest) {
        return ResponseStatusDto.success(
                workflowTaskService.getMyProcessInstances(pageRequest)
        );
    }

    @ApiOperation("获取流程节点")
    @RestGetMapping("/getNodeIds/{proInsId}")
    public ResponseStatusDto<WorkflowNodesDto> getNodeIds(
            @PathVariable String proInsId) {
        return ResponseStatusDto.success(
                workflowTaskService.getNodeIds(proInsId)
        );
    }

    @ApiOperation("获取流程待办任务列表")
    @RestPostMapping("/getTodoTaskList")
    public ResponseStatusDto<PageData<WorkflowTaskEntity>> getTodoTaskList(
            @RequestBody PageRequest<WorkflowQueryDto> pageRequest) {
        return ResponseStatusDto.success(
                workflowTaskService.getTodoTaskList(pageRequest)
        );
    }

    @ApiOperation("获取流程完成任务列表")
    @RestPostMapping("/getCompleteTaskList")
    public ResponseStatusDto<PageData<WorkflowTaskEntity>> getCompleteTaskList(
            @RequestBody PageRequest<WorkflowQueryDto> pageRequest) {
        return ResponseStatusDto.success(
                workflowTaskService.getCompleteTaskList(pageRequest)
        );
    }

    @ApiOperation("签收任务")
    @RestPostMapping("/claimTask/{taskId}/{claimUser}")
    public ResponseStatusDto<Void> claimTask(
            @PathVariable String taskId,
            @PathVariable String claimUser) {
        workflowTaskService.claimTask(taskId, claimUser);
        return ResponseStatusDto.success();
    }

    @ApiOperation("委托任务")
    @RestPostMapping("/delegateTask/{taskId}/{delegationUser}")
    public ResponseStatusDto<Void> delegateTask(
            @PathVariable String taskId,
            @PathVariable String delegationUser) {
        workflowTaskService.delegateTask(taskId, delegationUser);
        return ResponseStatusDto.success();
    }

    @ApiOperation("完成任务")
    @RestPostMapping("/completeTask/{taskId}")
    public ResponseStatusDto<Void> completeTask(
            @PathVariable String taskId,
            @RequestBody WorkflowVariableDto dto) {
        workflowTaskService.completeTask(taskId, dto);
        return ResponseStatusDto.success();
    }
}
