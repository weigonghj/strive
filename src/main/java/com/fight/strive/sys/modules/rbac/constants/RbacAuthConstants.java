package com.fight.strive.sys.modules.rbac.constants;

/**
 * @author ZHOUXIANG
 */
public class RbacAuthConstants {

    /**
     * auth_id -> auth entity
     */
    public static final String AUTH_ID_KEY = "sys_rbac_auth:auth_id";

    /**
     * auth_code -> auth_id，不同租户的 code 可能相同
     */
    public static final String AUTH_CODE_ID_KEY = "sys_rbac_auth:auth_code_id:";

    /**
     * roleId -> auth entity list
     */
    public static final String ROLE_AUTH_LIST_KEY = "sys_rbac_role_auth:role_auth_list";

    /**
     * roleId -> auth code list
     */
    public static final String ROLE_AUTH_CODES_KEY = "sys_rbac_role_auth:role_auth_codes";

    /**
     * roleId -> auth id list
     */
    public static final String ROLE_AUTH_IDS_KEY = "sys_rbac_role_auth:role_auth_ids";

    /**
     * menuId -> menu entity
     */
    public static final String MENU_ID_KEY = "sys_rbac_menu:menu_id";

    /**
     * menu code -> menu id， 注意区分租户
     */
    public static final String MENU_CODE_ID_KEY = "sys_rbac_menu:menu_code_id:";

    /**
     * elem Id -> elem entity
     */
    public static final String ELEM_ID_KEY = "sys_rbac_elem:elem_id";

    /**
     * elem code -> elem id， 注意区分租户
     */
    public static final String ELEM_CODE_ID_KEY = "sys_rbac_elem:elem_code_id:";

    /**
     * res Id -> res entity
     */
    public static final String RES_ID_KEY = "sys_rbac_res:res_id";

    /**
     * res code -> res id， 注意区分租户
     */
    public static final String RES_CODE_ID_KEY = "sys_rbac_res:res_code_id:";

    /**
     * auth_id -> auth resource entity list
     */
    public static final String AUTH_RES_LIST_KEY = "sys_rbac_auth_res:auth_res_list";

    /**
     * auth_id -> auth resource code set
     */
    public static final String AUTH_RES_CODES_KEY = "sys_rbac_auth_res:auth_res_codes";

    /**
     * auth_id -> auth resource id list
     */
    public static final String AUTH_RES_IDS_KEY = "sys_rbac_auth_res:auth_res_ids";
}
