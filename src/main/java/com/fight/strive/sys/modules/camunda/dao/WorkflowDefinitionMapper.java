package com.fight.strive.sys.modules.camunda.dao;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.fight.strive.sys.modules.camunda.entity.WorkflowDefinitionEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author ZHOUXIANG
 */
public interface WorkflowDefinitionMapper
        extends BaseMapper<WorkflowDefinitionEntity> {

    /**
     * 分页查询流程定义 SqlParser主要是过滤 mybatisplus 默认的租户行为，
     * 手动设置租户
     *
     * @param page    分页
     * @param queryWrapper 条件
     * @return 结果
     */
    @SqlParser(filter = true)
    @Select("SELECT t1.* from `act_re_procdef` t1 INNER JOIN (SELECT KEY_, max(VERSION_) as VERSION_ FROM `act_re_procdef` group by KEY_) t2 ON t1.KEY_ = t2.KEY_ and t1.VERSION_ = t2.VERSION_ ${ew.customSqlSegment}")
    IPage<WorkflowDefinitionEntity> listByPage(
            IPage<WorkflowDefinitionEntity> page,
            @Param(Constants.WRAPPER) QueryWrapper<WorkflowDefinitionEntity> queryWrapper);
}
