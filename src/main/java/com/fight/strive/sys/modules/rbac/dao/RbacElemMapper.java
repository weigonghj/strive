package com.fight.strive.sys.modules.rbac.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fight.strive.sys.modules.rbac.entity.RbacElemEntity;

/**
 * @author ZHOUXIANG
 */
public interface RbacElemMapper extends BaseMapper<RbacElemEntity> {
}
