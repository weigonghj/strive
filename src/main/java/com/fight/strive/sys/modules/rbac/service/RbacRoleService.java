package com.fight.strive.sys.modules.rbac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fight.strive.sys.modules.rbac.dto.RbacRoleDto;
import com.fight.strive.sys.modules.rbac.entity.RbacRoleEntity;
import com.fight.strive.sys.modules.request.dto.PageRequest;
import com.fight.strive.sys.modules.response.dto.PageData;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

/**
 * @author ZHOUXIANG
 */
@Validated
public interface RbacRoleService extends IService<RbacRoleEntity> {

    /**
     * 保存角色
     *
     * @param rbacRoleDto 角色接口模型
     */
    void saveRole(@Valid RbacRoleDto rbacRoleDto);

    /**
     * 查询代码是否重复
     *
     * @param roleCode 角色代码
     * @param id       ID
     * @return 角色信息
     */
    RbacRoleEntity getByCodeAndNotId(String roleCode, Long id);

    /**
     * 分页查询角色信息
     *
     * @param pageRequest 查询条件
     * @return 分页数据
     */
    PageData<RbacRoleEntity> getRoleList(PageRequest<RbacRoleDto> pageRequest);

    /**
     * 删除角色
     *
     * @param rbacRoleDto 角色接口模型
     */
    void deleteRole(RbacRoleDto rbacRoleDto);

    /**
     * 批量删除角色
     *
     * @param ids 多个ID
     */
    void deleteBatchRole(List<Long> ids);

    /**
     * 根据 id 获取 role entity
     *
     * @param roleId role id
     * @return role entity
     */
    RbacRoleEntity getRoleById(Long roleId);

    /**
     * 根据角色代码查询角色数据
     *
     * @param roleCode role code
     * @return role entity
     */
    RbacRoleEntity getRoleByCode(String roleCode);

    /**
     * 更新 redis 数据
     *
     * @param entity role entity
     */
    void updateRedisData(RbacRoleEntity entity);

    /**
     * 获取 redis 数据
     *
     * @param key     redis key
     * @param hashKey hash key
     * @return 获取 role entity
     */
    RbacRoleEntity getRedisData(String key, Object hashKey);

    /**
     * 删除 redis
     *
     * @param entity role entity
     */
    void deleteRedisData(RbacRoleEntity entity);

}
