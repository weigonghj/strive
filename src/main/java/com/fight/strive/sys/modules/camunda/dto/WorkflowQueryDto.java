package com.fight.strive.sys.modules.camunda.dto;

import com.fight.strive.sys.utils.StringUtils;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author ZHOUXIANG
 */
@Data
@Accessors(chain = true)
@ApiModel
public class WorkflowQueryDto {

    /**
     * 流程名称
     */
    private String processDefinitionName;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 候选组（以逗号分隔）
     */
    private String candidateGroups;

    /**
     * 候选用户（以逗号分隔）
     */
    private String candidateUsers;

    /**
     * 任务拥有者
     */
    private String owner;

    /**
     * 签收的用户
     */
    private String assignee;

    /**
     * 流程发起人
     */
    private String startUser;

    /**
     * 流程/任务是否结束
     */
    private String isFinished;

    /**
     * 获取字符串列表
     *
     * @return 列表
     */
    public List<String> getGroupIdList() {
        List<String> ids = new ArrayList<>();
        if (StringUtils.isNotBlank(this.candidateGroups)) {
            String[] idarr = StringUtils.split(this.candidateGroups, ",");
            Collections.addAll(ids, idarr);
        }
        return ids;
    }

    /**
     * 获取候选用户ID列表
     *
     * @return id列表
     */
    public List<String> getUserIdList() {
        List<String> ids = new ArrayList<>();
        if (StringUtils.isNotBlank(this.candidateUsers)) {
            String[] idarr = StringUtils.split(this.candidateUsers, ",");
            Collections.addAll(ids, idarr);
        }
        return ids;
    }
}
