package com.fight.strive.sys.modules.redis.config;

import com.alibaba.fastjson.support.spring.GenericFastJsonRedisSerializer;
import com.fight.strive.sys.modules.redis.subscribe.RedisSubscribe;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * @author ZHOUXIANG
 */
@Configuration
public class RedisConfig {

    /**
     * 重新配置RedisTemplate，设置相关的序列化
     */
    @Bean
    public RedisTemplate<String, Object> redisTemplate(
            RedisConnectionFactory redisConnectionFactory,
            RedisSerializer<Object> jsonRedisSerializer) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        // 设置value的序列化规则和 key的序列化规则
        redisTemplate.setKeySerializer(jsonRedisSerializer);
        redisTemplate.setHashKeySerializer(jsonRedisSerializer);
        redisTemplate.setValueSerializer(jsonRedisSerializer);
        redisTemplate.setHashValueSerializer(jsonRedisSerializer);
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }

    /**
     * 通过自定义配置构建Redis的Json序列化器
     */
    @Bean
    public RedisSerializer<Object> jsonRedisSerializer() {
        return new GenericFastJsonRedisSerializer();
    }

    /**
     * redis 事件监听
     */
    @Bean
    RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        // 可以添加多个 messageListener，配置不同的交换机
        // 使用此方法发送主题消息，<code>redisTemplate.convertAndSend("topic:test", "hello world!");</code>
        // 即可以 RedisSubscribe 中接收到消息
        container.addMessageListener(
                new MessageListenerAdapter(
                        new RedisSubscribe(),
                        "onMessage"),
                new PatternTopic("topic:test"));
        return container;
    }

}
