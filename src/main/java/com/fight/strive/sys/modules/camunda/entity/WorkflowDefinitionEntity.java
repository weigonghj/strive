package com.fight.strive.sys.modules.camunda.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 自定义访问camunda流程定义表，方便查询
 *
 * @author ZHOUXIANG
 */
@Data
@Accessors(chain = true)
@TableName("ACT_RE_PROCDEF")
public class WorkflowDefinitionEntity {

    @TableId("ID_")
    private String id;

    @TableField("NAME_")
    private String name;

    @TableField("KEY_")
    private String key;

    @TableField("VERSION_")
    protected int version;

    @TableField("CATEGORY_")
    protected String category;

    @TableField("DEPLOYMENT_ID_")
    protected String deploymentId;

    @TableField("RESOURCE_NAME_")
    protected String resourceName;

    @TableField("TENANT_ID_")
    private String tenantId;

    @TableField("VERSION_TAG_")
    private String versionTag;
}
