package com.fight.strive.sys.modules.rbac.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fight.strive.sys.modules.exception.StriveException;
import com.fight.strive.sys.modules.mybatisplus.utils.MyBatisPlusUtils;
import com.fight.strive.sys.modules.rbac.constants.RbacAuthConstants;
import com.fight.strive.sys.modules.rbac.dao.RbacResMapper;
import com.fight.strive.sys.modules.rbac.dto.RbacResDto;
import com.fight.strive.sys.modules.rbac.entity.RbacResEntity;
import com.fight.strive.sys.modules.rbac.service.RbacResService;
import com.fight.strive.sys.modules.request.dto.PageRequest;
import com.fight.strive.sys.modules.response.dto.PageData;
import com.fight.strive.sys.modules.sysadmin.utils.SysAdminUtils;
import com.fight.strive.sys.utils.BeanUtils;
import com.fight.strive.sys.utils.ObjectUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * @author ZHOUXIANG
 */
@Service
@Slf4j
public class RbacResServiceImpl
        extends ServiceImpl<RbacResMapper, RbacResEntity>
        implements RbacResService {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public void save(@Valid RbacResDto dto) {
        RbacResEntity entity = this.getByCodeNotId(
                dto.getResUri(), dto.getId());
        if (ObjectUtils.notNull(entity)) {
            throw new StriveException("该资源代码已经存在");
        }
        entity = new RbacResEntity();
        BeanUtils.copyPropertiesIgnoreNull(dto, entity);
        this.saveOrUpdate(entity);
        // 更新缓存
        this.updateRedisData(entity);
    }

    @Override
    public RbacResEntity getByCodeNotId(String code, Long id) {
        QueryWrapper<RbacResEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("res_uri", code);
        if (ObjectUtils.notNull(id)) {
            queryWrapper.ne("id", id);
        }
        return this.getOne(queryWrapper);
    }

    @Override
    public PageData<RbacResEntity> list(PageRequest<RbacResDto> pageRequest) {
        QueryWrapper<RbacResEntity> queryWrapper = new QueryWrapper<>();
        MyBatisPlusUtils.buildQuery(queryWrapper, pageRequest);
        IPage<RbacResEntity> page = pageRequest.buildMybatisPlusPage();
        IPage<RbacResEntity> iPage = this.page(page, queryWrapper);
        return new PageData<>(iPage);
    }

    @Override
    public void delete(RbacResDto dto) {
        // 删除缓存
        this.deleteRedisData(this.getResById(dto.getId()));
        // 删除数据库
        this.removeById(dto.getId());
    }

    @Override
    public void deleteBatch(List<Long> ids) {
        // 删除缓存
        for (Long id : ids) {
            RbacResEntity entity = this.getResById(id);
            this.deleteRedisData(entity);
        }
        // 删除数据库
        this.removeByIds(ids);
    }

    @Override
    public RbacResEntity getResById(Long resId) {
        RbacResEntity entity = this.getRedisData(
                RbacAuthConstants.RES_ID_KEY, resId);
        if (ObjectUtils.isNull(entity)) {
            // 读取数据库
            entity = this.getById(resId);
            // 存入 redis
            this.updateRedisData(entity);
        }
        return entity;
    }

    @Override
    public RbacResEntity getResByCode(String resUri) {
        Long tenantId = SysAdminUtils.getCurrentTenantId();
        RbacResEntity entity = this.getRedisData(
                RbacAuthConstants.RES_CODE_ID_KEY
                        .concat(String.valueOf(tenantId)), resUri);
        if (ObjectUtils.isNull(entity)) {
            QueryWrapper<RbacResEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("res_uri", resUri);
            entity = this.getOne(queryWrapper, false);
            if (ObjectUtils.notNull(entity)) {
                this.updateRedisData(entity);
            }
        }
        return entity;
    }

    @Override
    public void updateRedisData(RbacResEntity entity) {
        // res id -> entity
        redisTemplate.opsForHash().put(
                RbacAuthConstants.RES_ID_KEY, entity.getId(), entity);
        // res code -> res id
        redisTemplate.opsForHash().put(
                RbacAuthConstants.RES_CODE_ID_KEY
                        .concat(String.valueOf(entity.getTenantId())),
                entity.getResUri(), entity.getId());
    }

    @Override
    public RbacResEntity getRedisData(String key, Object hashKey) {
        if (RbacAuthConstants.RES_ID_KEY.equals(key)) {
            return (RbacResEntity) redisTemplate.opsForHash().get(
                    RbacAuthConstants.RES_ID_KEY, hashKey);
        } else {
            Long resId = (Long) redisTemplate.opsForHash().get(key, hashKey);
            if (ObjectUtils.notNull(resId)) {
                return (RbacResEntity) redisTemplate.opsForHash().get(
                        RbacAuthConstants.RES_ID_KEY, resId);
            }
        }
        return null;
    }

    @Override
    public void deleteRedisData(RbacResEntity entity) {
        redisTemplate.opsForHash().delete(
                RbacAuthConstants.RES_ID_KEY, entity.getId());
        redisTemplate.opsForHash().delete(
                RbacAuthConstants.RES_CODE_ID_KEY
                        .concat(String.valueOf(entity.getTenantId())),
                entity.getResUri());
    }
}
