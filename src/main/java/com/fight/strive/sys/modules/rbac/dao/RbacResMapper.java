package com.fight.strive.sys.modules.rbac.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fight.strive.sys.modules.rbac.entity.RbacResEntity;

/**
 * @author ZHOUXIANG
 */
public interface RbacResMapper extends BaseMapper<RbacResEntity> {
}
