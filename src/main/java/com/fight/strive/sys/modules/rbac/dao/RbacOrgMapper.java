package com.fight.strive.sys.modules.rbac.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fight.strive.sys.modules.rbac.entity.RbacOrgEntity;

/**
 * @author ZHOUXIANG
 */
public interface RbacOrgMapper extends BaseMapper<RbacOrgEntity> {
}
