package com.fight.strive.sys.modules.security.service;

import com.fight.strive.sys.modules.rbac.entity.RbacUserEntity;
import com.fight.strive.sys.modules.rbac.service.RbacUserService;
import com.fight.strive.sys.modules.security.dto.StriveUserDetails;
import com.fight.strive.sys.utils.ObjectUtils;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author ZHOUXIANG
 */
@Component
@Primary
public class UserDetailsServiceImpl implements UserDetailsService {

    @Resource
    private RbacUserService rbacUserService;

    @Override
    public UserDetails loadUserByUsername(String userName)
            throws UsernameNotFoundException {
        StriveUserDetails user = new StriveUserDetails();
        RbacUserEntity rbacUserEntity =
                rbacUserService.getUserByLoginName(userName);
        if (ObjectUtils.notNull(rbacUserEntity)) {
            user.setUser(rbacUserEntity);
        }
        return user;
    }

}
