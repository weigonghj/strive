package com.fight.strive.sys.modules.camunda.controller;

import com.fight.strive.sys.modules.camunda.dto.WorkflowDefinitionDto;
import com.fight.strive.sys.modules.camunda.dto.WorkflowQueryDto;
import com.fight.strive.sys.modules.camunda.entity.WorkflowDefinitionEntity;
import com.fight.strive.sys.modules.camunda.service.WorkflowDefinitionService;
import com.fight.strive.sys.modules.request.annotation.RestGetMapping;
import com.fight.strive.sys.modules.request.annotation.RestPostMapping;
import com.fight.strive.sys.modules.request.dto.PageRequest;
import com.fight.strive.sys.modules.response.dto.PageData;
import com.fight.strive.sys.modules.response.dto.ResponseStatusDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * @author ZHOUXIANG
 */
@Controller
@RequestMapping("/admin/sys/workflow/definition")
@Api("流程定义接口")
public class WorkflowDefinitionController {

    @Resource
    private WorkflowDefinitionService workflowDefinitionService;

    @ApiOperation("分页获取流程定义列表")
    @RestPostMapping("/list")
    public ResponseStatusDto<PageData<WorkflowDefinitionDto>> listDefinitionByPage(
            @RequestBody PageRequest<WorkflowDefinitionDto> pageRequest) {
        return ResponseStatusDto.success(
                workflowDefinitionService.listDefinitionByPage(pageRequest)
        );
    }

    @ApiOperation("根据用户ID分页获取流程定义列表")
    @RestPostMapping("/listProcess")
    public ResponseStatusDto<PageData<WorkflowDefinitionEntity>> listProcessByPage(
            @RequestBody PageRequest<WorkflowQueryDto> pageRequest) {
        return ResponseStatusDto.success(
                workflowDefinitionService.listProcessByPage(pageRequest)
        );
    }

    @ApiOperation("流程定义删除")
    @RestPostMapping("/delete")
    public ResponseStatusDto<Void> deleteProcessDefinition(
            @RequestBody WorkflowDefinitionDto dto) {
        workflowDefinitionService.deleteProcessDefinition(dto);
        return ResponseStatusDto.success();
    }

    @ApiOperation("流程资源下载")
    @RestPostMapping("/download")
    public void downloadResourceFile(@RequestBody WorkflowDefinitionDto dto) {
        workflowDefinitionService.downloadResourceFile(dto);
    }

    @ApiOperation("流程图bpmn xml 字符串")
    @RestGetMapping("/bpmnXml/{id}")
    public ResponseStatusDto<String> getDefinitionXmlString(@PathVariable String id) {
        return ResponseStatusDto.success(
                workflowDefinitionService.getDefinitionXmlString(id)
        );
    }
}
