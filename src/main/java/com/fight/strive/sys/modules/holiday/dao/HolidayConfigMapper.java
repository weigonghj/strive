package com.fight.strive.sys.modules.holiday.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fight.strive.sys.modules.holiday.entity.HolidayConfigEntity;

/**
 * @author ZHOUXIANG
 */
public interface HolidayConfigMapper extends BaseMapper<HolidayConfigEntity> {
}
