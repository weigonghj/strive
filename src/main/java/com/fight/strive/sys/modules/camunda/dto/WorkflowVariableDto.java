package com.fight.strive.sys.modules.camunda.dto;

import com.fight.strive.sys.utils.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author ZHOUXIANG
 */
@Data
@Accessors(chain = true)
@ApiModel("流程数据")
public class WorkflowVariableDto {

    @ApiModelProperty("表单或流程数据")
    Map<String, Object> variables;

    @ApiModelProperty("流程发起人")
    private String startUser;

    @ApiModelProperty("流程当前节点")
    private String assignee;

    @ApiModelProperty("流程当前节点的候选用户")
    private String candidateUsers;

    @ApiModelProperty("流程当前节点的候选组")
    private String candidateGroups;

    /**
     * 获取字符串列表
     *
     * @param idstr 以逗号分隔的字符串
     * @return 列表
     */
    public static List<String> getIdListByStr(String idstr) {
        List<String> ids = new ArrayList<>();
        if (StringUtils.isNotBlank(idstr)) {
            String[] idarr = StringUtils.split(idstr, ",");
            Collections.addAll(ids, idarr);
        }
        return ids;
    }
}
