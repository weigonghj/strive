package com.fight.strive.sys.modules.camunda.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author ZHOUXIANG
 */
@Data
@Accessors(chain = true)
@ApiModel("流程定义接口对象")
public class WorkflowDefinitionDto {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("key")
    private String key;

    @ApiModelProperty("name")
    private String name;

    @ApiModelProperty("version")
    private int version;

    @ApiModelProperty("versionTag")
    private String versionTag;

    @ApiModelProperty("category")
    private String category;

    @ApiModelProperty("deploymentId")
    private String deploymentId;

    @ApiModelProperty("resourceName")
    private String resourceName;

    @ApiModelProperty("diagramResourceName")
    private String diagramResourceName;

    @ApiModelProperty("tenantId")
    private String tenantId;

}
