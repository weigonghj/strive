package com.fight.strive.sys.modules.camunda.dao;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.fight.strive.sys.modules.camunda.entity.WorkflowTaskEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author ZHOUXIANG
 */
public interface WorkflowTaskMapper extends BaseMapper<WorkflowTaskEntity> {

    /**
     * 查询待办任务
     *
     * @param page         分页组件
     * @param queryWrapper 条件查询
     * @return 待办任务
     */
    @SqlParser(filter = true)
    @Select("SELECT * from (SELECT t1.*, t2.NAME_ as PROC_DEF_NAME_ FROM `act_ru_task` t1 INNER JOIN `act_re_procdef` t2 on t1.PROC_DEF_ID_ = t2.ID_) t ${ew.customSqlSegment}")
    IPage<WorkflowTaskEntity> listTodoTaskByPage(
            IPage<WorkflowTaskEntity> page,
            @Param(Constants.WRAPPER) QueryWrapper<WorkflowTaskEntity> queryWrapper);

    /**
     * 查询已办任务
     *
     * @param page         分页组件
     * @param queryWrapper 条件查询
     * @return 待办任务
     */
    @SqlParser(filter = true)
    @Select("SELECT * from (SELECT t1.*, t2.NAME_ as PROC_DEF_NAME_ FROM `act_hi_taskinst` t1 INNER JOIN `act_re_procdef` t2 on t1.PROC_DEF_ID_ = t2.ID_) t ${ew.customSqlSegment}")
    IPage<WorkflowTaskEntity> listCompleteTaskByPage(
            IPage<WorkflowTaskEntity> page,
            @Param(Constants.WRAPPER) QueryWrapper<WorkflowTaskEntity> queryWrapper);
}
