package com.fight.strive.sys.modules.request.enums;

/**
 * 排序枚举
 *
 * @author ZHOUXIANG
 */
@SuppressWarnings("unused")
public enum SortOrderEnum {

    /**
     * 升序
     */
    ASC("asc"),
    /**
     * 降序
     */
    DESC("desc");

    SortOrderEnum(String name) {
    }
}
