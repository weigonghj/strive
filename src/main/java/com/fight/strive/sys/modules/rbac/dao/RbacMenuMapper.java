package com.fight.strive.sys.modules.rbac.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fight.strive.sys.modules.rbac.entity.RbacMenuEntity;

/**
 * @author ZHOUXIANG
 */
public interface RbacMenuMapper extends BaseMapper<RbacMenuEntity> {
}
