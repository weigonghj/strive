package com.fight.strive.sys.modules.oauth2.service.impl;

import com.fight.strive.sys.enums.AuthTypeEnum;
import com.fight.strive.sys.enums.SuperAdminEnum;
import com.fight.strive.sys.modules.exception.StriveNoAuthException;
import com.fight.strive.sys.modules.oauth2.dto.LoginUserInfoDto;
import com.fight.strive.sys.modules.oauth2.service.Oauth2Service;
import com.fight.strive.sys.modules.rbac.entity.RbacUserEntity;
import com.fight.strive.sys.modules.rbac.service.RbacAuthResourceService;
import com.fight.strive.sys.modules.rbac.service.RbacOrgService;
import com.fight.strive.sys.modules.rbac.service.RbacRoleAuthService;
import com.fight.strive.sys.modules.rbac.service.RbacUserRoleService;
import com.fight.strive.sys.modules.rbac.service.RbacUserService;
import com.fight.strive.sys.modules.sysadmin.utils.SysAdminUtils;
import com.fight.strive.sys.utils.ObjectUtils;
import com.fight.strive.sys.utils.StringUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author ZHOUXIANG
 */
@Service
@Slf4j
@AllArgsConstructor
public class Oauth2ServiceImpl implements Oauth2Service {

    private final RbacUserService rbacUserService;

    private final RbacOrgService rbacOrgService;

    private final RbacUserRoleService rbacUserRoleService;

    private final RbacRoleAuthService rbacRoleAuthService;

    private final RbacAuthResourceService rbacAuthResourceService;

    @Override
    public LoginUserInfoDto getLoginUserInfo() {
        LoginUserInfoDto dto = new LoginUserInfoDto();
        String userName = SysAdminUtils.getCurrentUserLoginName();
        if (ObjectUtils.isNull(userName)) {
            throw new StriveNoAuthException("没有权限获取当前用户");
        }
        // 用户基本信息
        RbacUserEntity user = rbacUserService.getUserByLoginName(userName);
        if (StringUtils.equals(userName, SuperAdminEnum.LOGIN_NAME.value)) {
            user = SysAdminUtils.getCurrentUserEntity();
        }
        if (ObjectUtils.isNull(user)) {
            throw new StriveNoAuthException("没有权限获取当前用户");
        }
        dto.setUser(user);
        // 组织信息
        if (ObjectUtils.notNull(user.getOrgId())) {
            dto.setOrgs(rbacOrgService
                    .getLevelOrgList(user.getOrgId()));
        }
        // 角色集合
        dto.setRoles(rbacUserRoleService
                .getUserRoles(user.getId()));
        // 权限集合
        dto.setAuths(rbacRoleAuthService
                .getUserRoleAuth(dto.getRoles()));
        // 菜单集合
        dto.setMenus(rbacAuthResourceService
                .getUserAuthResource(
                        dto.getAuths(), AuthTypeEnum.MENU.name()));
        // 元素集合
        dto.setElements(rbacAuthResourceService
                .getUserAuthResource(
                        dto.getAuths(), AuthTypeEnum.ELEMENT.name()));
        // 资源集合
        dto.setResources(rbacAuthResourceService
                .getUserAuthResource(
                        dto.getAuths(), AuthTypeEnum.RESOURCE.name()));
        return dto;
    }
}
