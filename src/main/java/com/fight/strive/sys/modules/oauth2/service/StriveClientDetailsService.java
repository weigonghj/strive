package com.fight.strive.sys.modules.oauth2.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fight.strive.sys.modules.oauth2.dto.ClientDetailsDto;
import com.fight.strive.sys.modules.oauth2.entity.ClientDetailsEntity;
import com.fight.strive.sys.modules.request.dto.PageRequest;
import com.fight.strive.sys.modules.response.dto.PageData;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

/**
 * @author ZHOUXIANG
 */
@Validated
public interface StriveClientDetailsService extends IService<ClientDetailsEntity> {

    /**
     * 根据客户端id获取客户端认证信息
     *
     * @param clientId 客户端ID
     * @return 客户端信息
     */
    ClientDetailsEntity getClientDetailsByClientId(String clientId);

    /**
     * 保存客户端认证信息
     *
     * @param entity 客户端信息实体
     */
    void saveClientDetails(@Valid ClientDetailsEntity entity);

    /**
     * 保存客户端认证信息
     *
     * @param dto 客户端信息接口模型
     */
    void saveClientDetails(@Valid ClientDetailsDto dto);

    /**
     * 分页查询客户端信息
     *
     * @param pageRequest 分页请求接口模型
     * @return 分页数据
     */
    PageData<ClientDetailsEntity> getClientDetailsList(
            PageRequest<ClientDetailsDto> pageRequest);

    /**
     * 删除客户端信息
     *
     * @param dto 接口模型
     */
    void deleteClientDetails(ClientDetailsDto dto);

    /**
     * 批量删除客户端信息
     *
     * @param ids 要删除的id列表
     */
    void deleteBatchClientDetails(List<Long> ids);

    /**
     * 退出登录
     */
    void oauth2Logout();

    /**
     * 根据登录名称注册登录
     *
     * @param loginName 登录名
     */
    void oauth2LogoutByLoginName(String loginName);

    /**
     * 根据 id 获取 client entity
     *
     * @param id id
     * @return entity
     */
    ClientDetailsEntity getClientById(Long id);

    /**
     * 更新缓存数据
     *
     * @param entity client details entity
     */
    void updateRedisData(ClientDetailsEntity entity);

    /**
     * 获取缓存数据
     *
     * @param key     redis key
     * @param hashKey hash key
     * @return client details entity
     */
    ClientDetailsEntity getRedisData(String key, Object hashKey);

    /**
     * 删除缓存数据
     *
     * @param entity client detail entity
     */
    void deleteRedisData(ClientDetailsEntity entity);
}
