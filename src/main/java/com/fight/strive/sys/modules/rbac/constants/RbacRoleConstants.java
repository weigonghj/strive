package com.fight.strive.sys.modules.rbac.constants;

/**
 * @author ZHOUXIANG
 */
public class RbacRoleConstants {

    /**
     * roleId -> roleEntity
     */
    public static final String ROLE_ID_KEY = "sys_rbac_role:role_id";

    /**
     * roleCode -> roleId，不同租户 roleCode 可能相同
     */
    public static final String ROLE_CODE_ID_KEY = "sys_rbac_role:role_code_id:";

    /**
     * userId -> roleId list
     */
    public static final String USER_ROLE_LIST_KEY = "sys_rbac_user_role:user_role_list";

    /**
     * userId -> roleCode list
     */
    public static final String USER_ROLE_CODES_KEY = "sys_rbac_user_role:user_role_codes";

    /**
     * userId -> roleId list
     */
    public static final String USER_ROLE_IDS_KEY = "sys_rbac_user_role:user_role_ids";

    /**
     * orgId -> roleId list
     */
    public static final String ORG_ROLE_LIST_KEY = "sys_rbac_org_role:org_role_list";

    /**
     * orgId -> roleCode list
     */
    public static final String ORG_ROLE_CODES_KEY = "sys_rbac_org_role:org_role_codes";

    /**
     * orgId -> roleId list
     */
    public static final String ORG_ROLE_IDS_KEY = "sys_rbac_org_role:org_role_ids";
}
