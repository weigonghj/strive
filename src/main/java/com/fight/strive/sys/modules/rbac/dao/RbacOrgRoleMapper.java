package com.fight.strive.sys.modules.rbac.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fight.strive.sys.modules.rbac.entity.RbacOrgRoleEntity;

/**
 * @author ZHOUXIANG
 */
public interface RbacOrgRoleMapper extends BaseMapper<RbacOrgRoleEntity> {
}
