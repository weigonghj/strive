package com.fight.strive.sys.modules.camunda.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.camunda.bpm.engine.history.HistoricActivityInstance;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZHOUXIANG
 */
@Data
@Accessors(chain = true)
@ApiModel("流程活动节点ID和sequenceId数据")
public class WorkflowNodesDto {

    @ApiModelProperty("活动节点ID")
    private List<HistoricActivityInstance> actIns = new ArrayList<>();

    @ApiModelProperty("活动节点关联sequence id")
    private List<String> seqIds = new ArrayList<>();
}
