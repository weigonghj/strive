package com.fight.strive.sys.modules.rbac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fight.strive.sys.modules.rbac.dto.RbacRoleAuthDto;
import com.fight.strive.sys.modules.rbac.entity.RbacRoleAuthEntity;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;

/**
 * @author ZHOUXIANG
 */
@Validated
public interface RbacRoleAuthService extends IService<RbacRoleAuthEntity> {

    /**
     * 为角色分配权限
     *
     * @param dto 权限信息
     */
    void save(@Valid RbacRoleAuthDto dto);

    /**
     * 删除该角色所有权限
     *
     * @param dto 权限信息
     */
    void delete(@Valid RbacRoleAuthDto dto);

    /**
     * 根据角色ID获取所有权限ID列表
     *
     * @param roleId 角色ID
     * @return 权限ID列表
     */
    List<Long> getAuthIds(Long roleId);

    /**
     * 获取用户所有的权限集合
     *
     * @param roleSet 角色集合
     * @return 权限集合
     */
    HashSet<String> getUserRoleAuth(HashSet<String> roleSet);

    /**
     * 获取角色对应的权限
     *
     * @param roleCode 角色代码
     * @return 权限代码
     */
    HashSet<String> getRoleAuth(String roleCode);

    /**
     * 根据角色ID获取权限列表
     *
     * @param roleId role id
     * @return role auth entity
     */
    List<RbacRoleAuthEntity> getRoleAuthByRoleId(Long roleId);

    /**
     * 根据 role code 获取 role auth list
     *
     * @param roleCode role code
     * @return role auth list
     */
    List<RbacRoleAuthEntity> getRoleAuthByRoleCode(String roleCode);

    /**
     * 更新角色权限缓存
     *
     * @param roleId 角色 id
     * @param auths  权限列表
     */
    void updateRedisData(Long roleId, List<RbacRoleAuthEntity> auths);

    /**
     * 删除 roleId 关联 权限缓存
     *
     * @param roleId 角色ID
     */
    void deleteRedisData(Long roleId);
}
