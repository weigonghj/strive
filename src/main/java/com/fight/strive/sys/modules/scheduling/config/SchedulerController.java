package com.fight.strive.sys.modules.scheduling.config;

import com.fight.strive.sys.modules.request.annotation.RestRequestMapping;
import com.fight.strive.sys.modules.response.dto.ResponseStatusDto;
import com.fight.strive.sys.modules.scheduling.enums.JobStatusEnum;
import com.fight.strive.sys.utils.ObjectUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * 调度器监控管理
 *
 * @author ZHOUXIANG
 */
@Controller
@RequestMapping("/admin/sys/sched/adm")
@Api
public class SchedulerController {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @ApiOperation("判断调度器是否运行,正常返回RUNNING，停止返回STOP")
    @RestRequestMapping("/running")
    public ResponseStatusDto<Object> isSchedRunning() {
        String status = (String) redisTemplate
                .opsForValue().get("monitor:scheduler");
        return ResponseStatusDto.success(
                ObjectUtils.isNull(status) ?
                        JobStatusEnum.STOP.name() : status);
    }
}
