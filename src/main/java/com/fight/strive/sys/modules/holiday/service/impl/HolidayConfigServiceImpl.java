package com.fight.strive.sys.modules.holiday.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fight.strive.sys.enums.TimeUnitEnum;
import com.fight.strive.sys.modules.exception.StriveException;
import com.fight.strive.sys.modules.holiday.dao.HolidayConfigMapper;
import com.fight.strive.sys.modules.holiday.dto.HolidayConfigDto;
import com.fight.strive.sys.modules.holiday.entity.HolidayConfigEntity;
import com.fight.strive.sys.modules.holiday.service.HolidayConfigService;
import com.fight.strive.sys.modules.mybatisplus.utils.MyBatisPlusUtils;
import com.fight.strive.sys.modules.sysadmin.utils.SysAdminUtils;
import com.fight.strive.sys.modules.request.dto.PageRequest;
import com.fight.strive.sys.modules.response.dto.PageData;
import com.fight.strive.sys.utils.BeanUtils;
import com.fight.strive.sys.utils.CollectionUtils;
import com.fight.strive.sys.utils.DateUtils;
import com.fight.strive.sys.utils.ObjectUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author ZHOUXIANG
 */
@Service
@Slf4j
public class HolidayConfigServiceImpl
        extends ServiceImpl<HolidayConfigMapper, HolidayConfigEntity> implements HolidayConfigService {

    @Override
    public void saveHolidayConfig(HolidayConfigDto dto) {
        // 提取年份数据
        Map<TimeUnitEnum, Integer> dateInfo = DateUtils.getInfoFromDateString(dto.getDate());
        dto.setYear(dateInfo.get(TimeUnitEnum.YEAR));
        HolidayConfigEntity entity = new HolidayConfigEntity();
        BeanUtils.copyPropertiesIgnoreNull(dto, entity);
        Long tenantId = SysAdminUtils.getCurrentTenantId();
        entity.setTenantId(tenantId);
        try {
            if (!this.saveOrUpdate(entity)) {
                throw new StriveException("保存节假日配置失败");
            }
        } catch (DuplicateKeyException e) {
            throw new StriveException("该节假日配置日期已存在");
        }
    }

    @Override
    public PageData<HolidayConfigEntity> getHolidayConfigList(PageRequest<HolidayConfigDto> pageRequest) {
        HolidayConfigDto dto = pageRequest.getCondition();
        if (ObjectUtils.isNull(dto.getYear())) {
            dto.setYear(DateUtils.getCurrentYear());
        }
        QueryWrapper<HolidayConfigEntity> queryWrapper = new QueryWrapper<>();
        MyBatisPlusUtils.buildQuery(queryWrapper, dto, "date", "type");
        MyBatisPlusUtils.buildSortOrderQuery(queryWrapper, pageRequest.getSort());
        Page<HolidayConfigEntity> page = pageRequest.buildMybatisPlusPage();
        IPage<HolidayConfigEntity> iPage = this.page(page, queryWrapper);
        return new PageData<>(iPage);
    }

    @Override
    public void deleteHolidayConfig(HolidayConfigDto dto) {
        if (ObjectUtils.isNull(dto.getId())) {
            throw new StriveException("节假日配置ID为空");
        }
        this.removeById(dto.getId());
    }

    @Override
    public void deleteBatchHolidayConfig(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            throw new StriveException("要删除的节假日配置ID为空");
        }
        this.removeByIds(ids);
    }
}
