package com.fight.strive.sys.modules.dict.constants;

/**
 * @author ZHOUXIANG
 */
public class DictConstants {

    /**
     * id -> entity
     */
    public static final String DICT_ID_KEY = "sys_dict_data:dict_id";

    /**
     * dict code -> dict id, 区分租户
     */
    public static final String CODE_ID_KEY = "sys_dict_data:code_value:";

    /**
     * dict code -> dict value，区分租户
     */
    public static final String CODE_VALUE_KEY = "sys_dict_data:code_value:";

    /**
     * parent_id -> child list
     */
    public static final String PARENT_ID_KEY = "sys_dict_data:parent_id";
}
