package com.fight.strive.sys.modules.cache.utils;

import com.fight.strive.sys.modules.web.component.SpringContextHolder;
import com.fight.strive.sys.utils.ObjectUtils;
import com.fight.strive.sys.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

/**
 * @author ZHOUXIANG
 */
@Slf4j
public class StriveCacheManagerUtils {

    /**
     * 手动缓存
     *
     * @param cacheName 缓存名称
     * @param key       缓存键
     * @param value     缓存值
     */
    public static void cacheValue(String cacheName, String key, Object value) {
        try {
            if (ObjectUtils.notNull(value)
                    && StringUtils.isNotBlank(cacheName)
                    && StringUtils.isNotBlank(key)) {
                CacheManager cacheManager =
                        SpringContextHolder.getBean(CacheManager.class);
                Cache cache = cacheManager.getCache(cacheName);
                if (ObjectUtils.notNull(cache)) {
                    cache.put(key, value);
                }
            }
        } catch (Exception e) {
            log.error("删除缓存报错:{}", e.getMessage());
        }
    }

    /**
     * 手动删除缓存
     *
     * @param cacheName 缓存名称
     * @param key       缓存键
     */
    public static void cacheEvict(String cacheName, String key) {
        try {
            if (StringUtils.isNotBlank(cacheName)
                    && StringUtils.isNotBlank(key)) {
                CacheManager cacheManager =
                        SpringContextHolder.getBean(CacheManager.class);
                Cache cache = cacheManager.getCache(cacheName);
                if (ObjectUtils.notNull(cache)) {
                    cache.evict(key);
                }
            }
        } catch (Exception e) {
            log.error("删除缓存报错:{}", e.getMessage());
        }
    }
}
