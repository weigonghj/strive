package com.fight.strive.sys.modules.errorlog.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fight.strive.sys.modules.errorlog.entity.ErrorLogEntity;

/**
 * @author ZHOUXIANG
 */
public interface ErrorLogMapper extends BaseMapper<ErrorLogEntity> {
}
