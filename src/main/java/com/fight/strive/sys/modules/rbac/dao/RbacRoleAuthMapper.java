package com.fight.strive.sys.modules.rbac.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fight.strive.sys.modules.rbac.entity.RbacRoleAuthEntity;

/**
 * @author ZHOUXIANG
 */
public interface RbacRoleAuthMapper extends BaseMapper<RbacRoleAuthEntity> {
}
