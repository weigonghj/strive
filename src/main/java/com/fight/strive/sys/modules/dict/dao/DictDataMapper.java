package com.fight.strive.sys.modules.dict.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fight.strive.sys.modules.dict.entity.DictDataEntity;

/**
 * @author ZHOUXIANG
 */
public interface DictDataMapper extends BaseMapper<DictDataEntity> {
}
