package com.fight.strive.sys.modules.accesslog.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fight.strive.sys.modules.accesslog.entity.AccessLogEntity;

/**
 * @author ZHOUXIANG
 */
public interface AccessLogMapper extends BaseMapper<AccessLogEntity> {
}
