package com.fight.strive.sys.modules.camunda.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author ZHOUXIANG
 */
@Data
@Accessors(chain = true)
public class WorkflowTaskEntity {

    private String id;

    private String name;

    private String description;

    private int priority;

    private String owner;

    private String assignee;

    private String delegation;

    private String procInstId;

    private String procDefId;

    private String procDefName;

    private String executionId;

    private String taskDefKey;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date dueDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date followUpDate;

    private String parentTaskId;

    private Boolean suspensionState;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;

    private String tenantId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
}
