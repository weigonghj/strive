package com.fight.strive.sys.modules.camunda.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.camunda.bpm.engine.form.FormField;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZHOUXIANG
 */
@Data
@Accessors(chain = true)
@ApiModel("流程设计表单数据")
public class WorkflowFormData {

    @ApiModelProperty("流程名称")
    private String processName;

    @ApiModelProperty("流程ID")
    private String processId;

    @ApiModelProperty("流程KEY")
    private String processKey;

    @ApiModelProperty("表单编号")
    private String formKey;

    @ApiModelProperty("部署ID")
    private String deploymentId;

    @ApiModelProperty("任务ID")
    private String taskId;

    @ApiModelProperty("任务名称")
    private String taskName;

    @ApiModelProperty("字段内容")
    private List<FormField> formFields = new ArrayList<>();
}
