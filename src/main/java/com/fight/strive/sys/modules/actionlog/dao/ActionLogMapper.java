package com.fight.strive.sys.modules.actionlog.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fight.strive.sys.modules.actionlog.entity.ActionLogEntity;

/**
 * @author ZHOUXIANG
 */
public interface ActionLogMapper extends BaseMapper<ActionLogEntity> {
}
