package com.fight.strive.sys.modules.rbac.constants;

/**
 * @author ZHOUXIANG
 */
public class RbacUserConstants {

    /**
     * userId -> userEntity
     */
    public static final String ID_KEY = "sys_rbac_user:user_id";

    /**
     * loginName -> userId
     */
    public static final String LOGIN_NAME_KEY = "sys_rbac_user:login_name";

    /**
     * userId -> tenantId
     */
    public static final String TENANT_ID_KEY = "sys_rbac_user:tenant_id";

    /**
     * mobile -> userId
     */
    public static final String MOBILE_KEY = "sys_rbac_user:mobile";

    /**
     * userId -> orgId
     */
    public static final String USER_ORG_ID_KEY = "sys_rbac_user:user_org_id";
}
