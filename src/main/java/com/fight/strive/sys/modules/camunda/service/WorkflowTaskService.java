package com.fight.strive.sys.modules.camunda.service;

import com.fight.strive.sys.modules.camunda.dto.WorkflowFormData;
import com.fight.strive.sys.modules.camunda.dto.WorkflowNodesDto;
import com.fight.strive.sys.modules.camunda.dto.WorkflowQueryDto;
import com.fight.strive.sys.modules.camunda.dto.WorkflowVariableDto;
import com.fight.strive.sys.modules.camunda.entity.WorkflowTaskEntity;
import com.fight.strive.sys.modules.request.dto.PageRequest;
import com.fight.strive.sys.modules.response.dto.PageData;
import org.camunda.bpm.engine.history.HistoricProcessInstance;

/**
 * @author ZHOUXIANG
 */
public interface WorkflowTaskService {

    /**
     * 获取启动表单数据
     *
     * @param pdId 流程定义ID
     * @return 表单数据
     */
    WorkflowFormData getStartFormData(String pdId);

    /**
     * 获取任务表单
     *
     * @param taskId 任务ID
     * @return 任务表单
     */
    WorkflowFormData getTaskFormData(String taskId);

    /**
     * 启动流程
     *
     * @param key 流程定义ID
     * @param dto 流程变量
     */
    void startProcess(String key, WorkflowVariableDto dto);

    /**
     * 获取我发起过的流程
     *
     * @param pageRequest 分页请求
     * @return 分页条件
     */
    PageData<HistoricProcessInstance> getMyProcessInstances(
            PageRequest<WorkflowQueryDto> pageRequest);

    /**
     * 根据流程实例的ID获取活动节点
     *
     * @param proInsId 实例ID
     * @return 节点数据
     */
    WorkflowNodesDto getNodeIds(String proInsId);

    /**
     * 查询待办任务
     *
     * @param pageRequest 查询条件
     * @return 我的任务
     */
    PageData<WorkflowTaskEntity> getTodoTaskList(
            PageRequest<WorkflowQueryDto> pageRequest);

    /**
     * 查询已办任务
     *
     * @param pageRequest 查询条件
     * @return 我的任务
     */
    PageData<WorkflowTaskEntity> getCompleteTaskList(
            PageRequest<WorkflowQueryDto> pageRequest);

    /**
     * 签收任务
     *
     * @param taskId    任务ID
     * @param claimUser 签收人
     */
    void claimTask(String taskId, String claimUser);

    /**
     * 任务委托
     *
     * @param taskId         任务ID
     * @param delegationUser 任务委派者
     */
    void delegateTask(String taskId, String delegationUser);

    /**
     * 完成任务
     *
     * @param taskId 任务ID
     * @param dto 流程任务变量
     */
    void completeTask(String taskId, WorkflowVariableDto dto);
}
