package com.fight.strive.sys.modules.rbac.constants;

/**
 * @author ZHOUXIANG
 */
public class RbacOrgConstants {

    /**
     * redis 存储 org tree 的 键
     */
    public static final String ORG_TREE_KEY = "sys_rbac_org:org_tree";

    /**
     * 租户顶层组织 tenantId -> orgId
     */
    public static final String TENANT_TOP_ORG_KEY = "sys_rbac_org:tenant_top_org";

    /**
     * 根据ID查询org, orgId -> orgEntity
     */
    public static final String ORG_KEY = "sys_rbac_org:org_id";

    /**
     * 获取组织ID对应的租户ID, orgId ->tenantId
     */
    public static final String ORG_TENANTE_ID_KEY = "sys_rbac_org:org_tenant_id";

    /**
     * orgCode -> orgId，区分租户ID，不同租户下的代码可以重复
     */
    public static final String ORG_CODE_KEY = "sys_rbac_org:org_code:";

    /**
     * userId -> orgId
     */
    public static final String USER_ORG_KEY = "sys_rbac_org:user_org";
}
