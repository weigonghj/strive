package com.fight.strive.sys.modules.oauth2.constants;

/**
 * @author ZHOUXIANG
 */
public class Oauth2Constants {

    /**
     * oauthId -> entity
     */
    public static final String OAUTH2_ID_KEY = "sys_auth_client:oauth_id";

    /**
     * client_id -> oauth id
     */
    public static final String CLIENT_OAUTH2_ID_KEY = "sys_auth_client:client_oauth_id";
}
