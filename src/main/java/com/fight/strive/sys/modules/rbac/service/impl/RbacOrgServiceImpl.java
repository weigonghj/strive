package com.fight.strive.sys.modules.rbac.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fight.strive.sys.enums.OrgTypeEnum;
import com.fight.strive.sys.modules.exception.StriveException;
import com.fight.strive.sys.modules.mybatisplus.utils.MyBatisPlusUtils;
import com.fight.strive.sys.modules.rbac.constants.RbacOrgConstants;
import com.fight.strive.sys.modules.rbac.dao.RbacOrgMapper;
import com.fight.strive.sys.modules.rbac.dto.OrgTreeDto;
import com.fight.strive.sys.modules.rbac.dto.RbacOrgDto;
import com.fight.strive.sys.modules.rbac.entity.RbacOrgEntity;
import com.fight.strive.sys.modules.rbac.entity.RbacUserEntity;
import com.fight.strive.sys.modules.rbac.service.RbacOrgService;
import com.fight.strive.sys.modules.rbac.service.RbacUserService;
import com.fight.strive.sys.modules.request.dto.PageRequest;
import com.fight.strive.sys.modules.response.dto.PageData;
import com.fight.strive.sys.modules.sysadmin.utils.SysAdminUtils;
import com.fight.strive.sys.utils.BeanUtils;
import com.fight.strive.sys.utils.CollectionUtils;
import com.fight.strive.sys.utils.ObjectUtils;
import com.fight.strive.sys.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author ZHOUXIANG
 */
@Slf4j
@Service
public class RbacOrgServiceImpl
        extends ServiceImpl<RbacOrgMapper, RbacOrgEntity>
        implements RbacOrgService {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Resource
    @Lazy
    private RbacUserService rbacUserService;

    @Override
    public void registerOrg(Long tenantId, @Valid RbacOrgDto dto) {
        if (ObjectUtils.isNull(tenantId)) {
            throw new StriveException("租户ID为空");
        }
        RbacOrgEntity entity = new RbacOrgEntity();
        // 设置租户ID
        entity.setTenantId(tenantId);
        entity.setOrgCode(dto.getOrgCode())
                .setOrgName(dto.getOrgName())
                .setAliasName(dto.getAliasName())
                .setOrgTypeCode(dto.getOrgTypeCode())
                .setOrgTypeName(OrgTypeEnum
                        .getAlias(dto.getOrgTypeCode()))
                .setParentId(-1L)
                .setSequence(0.0);
        try {
            // 保存组织
            this.saveOrUpdate(entity);
            // 存到 redis 中，租户顶层组织
            this.updateRedisData(entity);
        } catch (DuplicateKeyException e) {
            throw new StriveException("组织代码重复");
        }
    }

    @Override
    public void saveOrg(@Valid RbacOrgDto rbacOrgDto) {
        RbacOrgEntity rbacOrgEntity = this.getByCodeAndNotId(
                rbacOrgDto.getOrgCode(), rbacOrgDto.getId());
        if (ObjectUtils.notNull(rbacOrgEntity)) {
            throw new StriveException("组织代码重复");
        } else {
            rbacOrgEntity = new RbacOrgEntity();
        }
        BeanUtils.copyProperties(rbacOrgDto, rbacOrgEntity);
        // 设置组织树ID字段
        RbacOrgEntity parent = this.getById(rbacOrgDto.getParentId());
        rbacOrgEntity.setTreeId(this.getTreeId(parent));
        // 设置组织类型名称
        rbacOrgEntity.setOrgTypeName(OrgTypeEnum
                .getAlias(rbacOrgDto.getOrgTypeCode()));
        // 保存组织
        this.saveOrUpdate(rbacOrgEntity);
        // 根据组织ID存入缓存
        this.updateRedisData(rbacOrgEntity);
    }

    @Override
    public RbacOrgEntity getByCodeAndNotId(String orgCode, Long id) {
        QueryWrapper<RbacOrgEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("org_code", orgCode);
        if (ObjectUtils.notNull(id)) {
            queryWrapper.ne("id", id);
        }
        return this.getOne(queryWrapper);
    }

    @Override
    public boolean hasChildOrg(Long parentId) {
        QueryWrapper<RbacOrgEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id", parentId);
        return this.count(queryWrapper) > 0;
    }

    @Override
    public PageData<RbacOrgEntity> getOrgList(PageRequest<RbacOrgDto> pageRequest) {
        QueryWrapper<RbacOrgEntity> queryWrapper = new QueryWrapper<>();
        MyBatisPlusUtils.buildQuery(queryWrapper, pageRequest, "orgCode");
        MyBatisPlusUtils.buildSortOrderQuery(queryWrapper, pageRequest.getSort());
        Page<RbacOrgEntity> page = pageRequest.buildMybatisPlusPage();
        IPage<RbacOrgEntity> iPage = this.page(page, queryWrapper);
        return new PageData<>(iPage);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteOrg(RbacOrgDto rbacOrgDto) {
        if (ObjectUtils.isNull(rbacOrgDto.getId())) {
            throw new StriveException("组织ID不能为空");
        }
        if (this.hasChildOrg(rbacOrgDto.getId())) {
            throw new StriveException("该组织下有子组织");
        }
        // 清缓存
        RbacOrgEntity entity = this.getOrgById(rbacOrgDto.getId());
        this.deleteRedisData(entity);
        // 删除
        this.removeById(rbacOrgDto.getId());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteBatchOrg(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            throw new StriveException("要删除的组织ID为空");
        }
        // 删除 redis 中的数据
        for (Long id : ids) {
            RbacOrgEntity entity = this.getOrgById(id);
            this.deleteRedisData(entity);
        }
        // 删除数据库中的数据
        this.removeByIds(ids);
    }

    @Override
    public void changeTopOrgInfo(RbacOrgDto dto) {
        Long tenantId = SysAdminUtils.getCurrentTenantId();
        RbacOrgEntity top = this.getTopOrgByTenantId();
        BeanUtils.copyPropertiesIgnoreNull(dto, top);
        this.saveOrUpdate(top);
        // 清缓存
        this.updateRedisData(top);
    }

    @Override
    public RbacOrgEntity getTopOrgByTenantId() {
        Long tenantId = SysAdminUtils.getCurrentTenantId();
        RbacOrgEntity entity = this.getRedisData(
                RbacOrgConstants.TENANT_TOP_ORG_KEY, tenantId);
        if (ObjectUtils.isNull(entity)) {
            QueryWrapper<RbacOrgEntity> queryWrapper = new QueryWrapper<>();
            // 当组织的父ID为空说是顶层组织，租户条件由框架统计在sql中添加，此处不用。
            queryWrapper.eq("parent_id", -1L);
            entity = this.getOne(queryWrapper);
            if (ObjectUtils.notNull(entity)) {
                this.updateRedisData(entity);
            }
        }
        return entity;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<OrgTreeDto> getOrgTree() {
        final List<OrgTreeDto> parents = new ArrayList<>();
        QueryWrapper<RbacOrgEntity> queryWrapper = new QueryWrapper<>();
        Long tenantId = SysAdminUtils.getCurrentTenantId();
        if (ObjectUtils.isNull(tenantId)) {
            throw new StriveException("无法获取租户ID");
        }
        // 首先检查缓存
        List<OrgTreeDto> cacheValue =
                (List<OrgTreeDto>) redisTemplate.opsForHash().get(
                        RbacOrgConstants.ORG_TREE_KEY, tenantId);
        if (ObjectUtils.notNull(cacheValue)) {
            return cacheValue;
        }
        RbacOrgEntity topOrg = this.getTopOrgByTenantId();
        if (ObjectUtils.notNull(topOrg)) {
            OrgTreeDto orgNode = new OrgTreeDto()
                    .setTitle(topOrg.getAliasName())
                    .setExpand(true)
                    .setData(topOrg);
            orgNode.setChildren(getOrgTreeChildren(topOrg.getId()));
            parents.add(orgNode);
            // 进行缓存
            redisTemplate.opsForHash().put(
                    RbacOrgConstants.ORG_TREE_KEY, tenantId, parents);
            return parents;
        }
        return Collections.emptyList();
    }

    @Override
    public Long getTenantIdByOrgId(Long orgId) {
        Long tenantId = (Long) redisTemplate.opsForHash().get(
                RbacOrgConstants.ORG_TENANTE_ID_KEY, orgId);
        if (ObjectUtils.isNull(tenantId)) {
            RbacOrgEntity entity = this.getById(orgId);
            if (ObjectUtils.isNull(entity)) {
                throw new StriveException("获取组织所属租户失败");
            }
            tenantId = entity.getTenantId();
            this.updateRedisData(entity);
        }
        // 返回组织租户
        return tenantId;
    }

    @Override
    public RbacOrgEntity getOrgByOrgCode(String orgCode) {
        Long tennantId = SysAdminUtils.getCurrentTenantId();
        RbacOrgEntity entity = this.getRedisData(
                RbacOrgConstants.ORG_CODE_KEY
                        .concat(String.valueOf(tennantId)), orgCode);
        if (ObjectUtils.isNull(entity)) {
            QueryWrapper<RbacOrgEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("org_code", orgCode);
            entity = this.getOne(queryWrapper, false);
            if (ObjectUtils.notNull(entity)) {
                this.updateRedisData(entity);
            }
        }
        return entity;
    }

    @Override
    public String getOrgNameByOrgCode(String orgCode) {
        RbacOrgEntity entity = this.getOrgByOrgCode(orgCode);
        return entity.getOrgName();
    }

    @Override
    public List<RbacOrgEntity> getOrgListByOrgType(String orgType) {
        QueryWrapper<RbacOrgEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("org_type_code", orgType)
                .orderByAsc("sequence");
        return this.list(queryWrapper);
    }

    @Override
    public List<RbacOrgEntity> getOrgListByParentId(Long parentId) {
        QueryWrapper<RbacOrgEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id", parentId)
                .orderByAsc("sequence");
        return this.list(queryWrapper);
    }

    @Override
    public List<RbacOrgEntity> getLevelOrgList(Long orgId) {
        List<RbacOrgEntity> orgList = new ArrayList<>();
        RbacOrgEntity bottomOrg = this.getById(orgId);
        if (ObjectUtils.notNull(bottomOrg)) {
            if (StringUtils.isNotBlank(bottomOrg.getTreeId())) {
                String[] treeIds = StringUtils.split(
                        bottomOrg.getTreeId(), ".");
                for (String treeId : treeIds) {
                    Long id = Long.parseLong(treeId);
                    orgList.add(this.getById(id));
                }
            }
            // 底层组织排在最后
            orgList.add(bottomOrg);
        }
        return orgList;
    }

    @Override
    public List<RbacOrgEntity> getUserLevelOrgList(Long userId) {
        RbacUserEntity userEntity;
        if (ObjectUtils.notNull(userId)) {
            userEntity = rbacUserService.getById(userId);
        } else {
            userEntity = SysAdminUtils.getCurrentUserEntity();
        }
        if (ObjectUtils.notNull(userEntity)) {
            return this.getLevelOrgList(userEntity.getOrgId());
        }
        return Collections.emptyList();
    }

    @Override
    public RbacOrgEntity getUserOrg(Long userId) {
        if (ObjectUtils.isNull(userId)) {
            RbacUserEntity userEntity =
                    SysAdminUtils.getCurrentUserEntity();
            if (ObjectUtils.notNull(userEntity)) {
                userId = userEntity.getId();
            }
        }
        if (ObjectUtils.notNull(userId)) {
            Long orgId = rbacUserService.getUserOrgId(userId);
            if (ObjectUtils.notNull(orgId)) {
                return this.getOrgById(orgId);
            }
        }
        return null;
    }

    @Override
    public RbacOrgEntity getOrgById(Long orgId) {
        RbacOrgEntity entity = this.getRedisData(
                RbacOrgConstants.ORG_KEY, orgId);
        if (ObjectUtils.isNull(entity)) {
            entity = this.getById(orgId);
            if (ObjectUtils.notNull(entity)) {
                this.updateRedisData(entity);
            }
        }
        return entity;
    }

    @Override
    public void updateRedisData(RbacOrgEntity entity) {
        // orgId -> orgEntity
        redisTemplate.opsForHash().put(
                RbacOrgConstants.ORG_KEY, entity.getId(), entity);
        redisTemplate.opsForHash().delete(
                RbacOrgConstants.ORG_TREE_KEY, entity.getTenantId());
        if (entity.getParentId().equals(-1L)) {
            // 顶层组织
            redisTemplate.opsForHash().put(
                    RbacOrgConstants.TENANT_TOP_ORG_KEY,
                    entity.getTenantId(), entity.getId());
        }
        // orgId -> tenantId
        redisTemplate.opsForHash().put(
                RbacOrgConstants.ORG_TENANTE_ID_KEY,
                entity.getId(), entity.getTenantId());
        // orgCode -> orgId
        redisTemplate.opsForHash().put(
                RbacOrgConstants.ORG_CODE_KEY
                        .concat(String.valueOf(entity.getTenantId())),
                entity.getOrgCode(), entity.getId());
    }

    @Override
    public void deleteRedisData(RbacOrgEntity entity) {
        redisTemplate.opsForHash().delete(
                RbacOrgConstants.ORG_KEY, entity.getId());
        redisTemplate.opsForHash().delete(
                RbacOrgConstants.ORG_TREE_KEY, entity.getTenantId());
        if (entity.getParentId().equals(-1L)) {
            // 顶层组织
            redisTemplate.opsForHash().delete(
                    RbacOrgConstants.TENANT_TOP_ORG_KEY,
                    entity.getTenantId());
        }
        // orgId -> tenantId
        redisTemplate.opsForHash().delete(
                RbacOrgConstants.ORG_TENANTE_ID_KEY, entity.getId());
        // orgCode -> orgId
        redisTemplate.opsForHash().delete(RbacOrgConstants.ORG_CODE_KEY
                .concat(String.valueOf(entity.getTenantId())),
                entity.getOrgCode());
    }

    @Override
    public RbacOrgEntity getRedisData(String key, Object hashKey) {
        if (RbacOrgConstants.ORG_KEY.equals(key)) {
            return (RbacOrgEntity) redisTemplate.opsForHash().get(
                    RbacOrgConstants.ORG_KEY, hashKey);
        } else {
            Object orgId = redisTemplate.opsForHash().get(key, hashKey);
            if (ObjectUtils.notNull(orgId)) {
                return (RbacOrgEntity) redisTemplate.opsForHash().get(
                        RbacOrgConstants.ORG_KEY, orgId);
            }
        }
        return null;
    }

    /**
     * 递归获取组织树
     *
     * @param parentId 父组织ID
     * @return 子组织列表
     */
    private List<OrgTreeDto> getOrgTreeChildren(Long parentId) {
        final List<OrgTreeDto> children = new ArrayList<>();
        QueryWrapper<RbacOrgEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id", parentId)
                .orderByAsc("sequence");
        List<RbacOrgEntity> orgList = this.list(queryWrapper);
        if (CollectionUtils.isNotEmpty(orgList)) {
            orgList.forEach(org -> {
                OrgTreeDto orgNode = new OrgTreeDto()
                        .setTitle(org.getAliasName())
                        .setData(org);
                List<OrgTreeDto> childrenNode = getOrgTreeChildren(org.getId());
                if (CollectionUtils.isNotEmpty(childrenNode)) {
                    orgNode.setChildren(childrenNode);
                }
                children.add(orgNode);
            });
        }
        return children;
    }

    /**
     * 从父组织中获取层级信息
     */
    private String getTreeId(RbacOrgEntity parent) {
        // 顶层组织的treeId为null
        if (ObjectUtils.isNull(parent.getTreeId())) {
            return String.valueOf(parent.getId());
        }
        return parent.getTreeId().concat(".")
                .concat(String.valueOf(parent.getId()));
    }
}
