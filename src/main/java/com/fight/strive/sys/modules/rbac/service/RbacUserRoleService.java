package com.fight.strive.sys.modules.rbac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fight.strive.sys.modules.rbac.dto.RbacUserRoleDto;
import com.fight.strive.sys.modules.rbac.entity.RbacUserRoleEntity;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;

/**
 * @author ZHOUXIANG
 */
@Validated
public interface RbacUserRoleService extends IService<RbacUserRoleEntity> {

    /**
     * 保存用户角色关系
     *
     * @param dto 用户角色关系
     */
    void save(@Valid RbacUserRoleDto dto);

    /**
     * 清除用户角色关系
     *
     * @param dto 用户角色关系
     */
    void delete(@Valid RbacUserRoleDto dto);

    /**
     * 获取用户关联角色ID列表
     *
     * @param userId 用户ID
     * @return 角色ID列表
     */
    List<Long> getIds(Long userId);

    /**
     * 获取用户所有角色
     *
     * @param userId 用户ID
     * @return 角色编码列表
     */
    HashSet<String> getUserRoles(Long userId);

    /**
     * 判断用户是否有该角色
     *
     * @param userId   用户ID
     * @param roleCode 角色代码
     * @return 是否有
     */
    boolean userHasRole(Long userId, String roleCode);

    /**
     * 获取用户角色关联信息
     *
     * @param userId user id
     * @return list user role
     */
    List<RbacUserRoleEntity> getUserRoleListByUserId(Long userId);

    /**
     * 更新 redis, userId -> roleIds 关系
     *
     * @param userId userId
     * @param roles  角色关联信息
     */
    void updateRedisData(Long userId, List<RbacUserRoleEntity> roles);

    /**
     * 删除用户角色缓存
     *
     * @param userId user id
     */
    void deleteRedisData(Long userId);

}
