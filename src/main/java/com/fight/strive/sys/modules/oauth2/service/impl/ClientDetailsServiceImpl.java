package com.fight.strive.sys.modules.oauth2.service.impl;

import com.fight.strive.sys.enums.StriveConstantsEnum;
import com.fight.strive.sys.modules.oauth2.client.ClientDetailsImpl;
import com.fight.strive.sys.modules.oauth2.entity.ClientDetailsEntity;
import com.fight.strive.sys.modules.oauth2.service.StriveClientDetailsService;
import com.fight.strive.sys.utils.JSONUtils;
import com.fight.strive.sys.utils.ObjectUtils;
import com.fight.strive.sys.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 自定义Oauth2认证客户端服务
 *
 * @author ZHOUXIANG
 */
@Slf4j
@Service
@Primary
public class ClientDetailsServiceImpl implements ClientDetailsService {

    @Resource
    private StriveClientDetailsService striveClientDetailsService;

    @Override
    public ClientDetails loadClientByClientId(String clientId)
            throws ClientRegistrationException {
        ClientDetailsEntity client =
                striveClientDetailsService.getClientDetailsByClientId(clientId);
        if (ObjectUtils.isNull(client)) {
            if (StringUtils.equalsIgnoreCase(
                    clientId, StriveConstantsEnum.STRIVE_APP_ID.value)) {
                client = new ClientDetailsEntity();
                client.setClientId(clientId)
                        // 为空不检查，不为空时就会检查client resource id是否可以访问此台资源服务
                        .setResourceIds("STRIVE_RESOURCE")
                        .setScopes("request")
                        .setAuthorizedGrantTypes("password")
                        .setClientSecret("6d930500293d11658b1727951c5a9489")
                        .setAccessTokenValiditySeconds(3600 * 24 * 365)
                        .setRefreshTokenValiditySeconds(60);
                striveClientDetailsService.saveClientDetails(client);
            } else {
                log.error("Oauth2认证未能根据客户端ID找到此客户端信息：{}", clientId);
            }
        }
        if (ObjectUtils.notNull(client)) {
            ClientDetailsImpl clientDetails = new ClientDetailsImpl(client);
            log.debug("Oauth2请求的客户端信息为：{}", JSONUtils.obj2Json(clientDetails));
            return clientDetails;
        }
        return null;
    }

}
