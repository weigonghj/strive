package com.fight.strive.sys.modules.rbac.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fight.strive.sys.modules.exception.StriveException;
import com.fight.strive.sys.modules.mybatisplus.utils.MyBatisPlusUtils;
import com.fight.strive.sys.modules.rbac.constants.RbacAuthConstants;
import com.fight.strive.sys.modules.rbac.dao.RbacMenuMapper;
import com.fight.strive.sys.modules.rbac.dto.RbacMenuDto;
import com.fight.strive.sys.modules.rbac.entity.RbacMenuEntity;
import com.fight.strive.sys.modules.rbac.service.RbacMenuService;
import com.fight.strive.sys.modules.request.dto.PageRequest;
import com.fight.strive.sys.modules.response.dto.PageData;
import com.fight.strive.sys.modules.sysadmin.utils.SysAdminUtils;
import com.fight.strive.sys.utils.BeanUtils;
import com.fight.strive.sys.utils.ObjectUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * @author ZHOUXIANG
 */
@Service
@Slf4j
public class RbacMenuServiceImpl
        extends ServiceImpl<RbacMenuMapper, RbacMenuEntity>
        implements RbacMenuService {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public void save(@Valid RbacMenuDto dto) {
        RbacMenuEntity entity = this.getByCodeNotId(
                dto.getMenuCode(), dto.getId());
        if (ObjectUtils.notNull(entity)) {
            throw new StriveException("该菜单限代码已经存在");
        }
        entity = new RbacMenuEntity();
        BeanUtils.copyPropertiesIgnoreNull(dto, entity);
        this.saveOrUpdate(entity);
        // 更新缓存
        this.updateRedisData(entity);
    }

    @Override
    public RbacMenuEntity getByCodeNotId(String code, Long id) {
        QueryWrapper<RbacMenuEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("menu_code", code);
        if (ObjectUtils.notNull(id)) {
            queryWrapper.ne("id", id);
        }
        return this.getOne(queryWrapper);
    }

    @Override
    public PageData<RbacMenuEntity> list(PageRequest<RbacMenuDto> pageRequest) {
        QueryWrapper<RbacMenuEntity> queryWrapper = new QueryWrapper<>();
        MyBatisPlusUtils.buildQuery(queryWrapper, pageRequest, "authCode");
        IPage<RbacMenuEntity> page = pageRequest.buildMybatisPlusPage();
        IPage<RbacMenuEntity> iPage = this.page(page, queryWrapper);
        return new PageData<>(iPage);
    }

    @Override
    public void delete(RbacMenuDto dto) {
        // 删除缓存
        this.deleteRedisData(this.getMenuById(dto.getId()));
        // 删除数据库
        this.removeById(dto.getId());
    }

    @Override
    public void deleteBatch(List<Long> ids) {
        // 删除缓存
        for (Long id : ids) {
            RbacMenuEntity entity = this.getMenuById(id);
            this.deleteRedisData(entity);
        }
        // 删除数据数
        this.removeByIds(ids);
    }

    @Override
    public RbacMenuEntity getMenuById(Long menuId) {
        RbacMenuEntity entity = this.getRedisData(
                RbacAuthConstants.MENU_ID_KEY, menuId);
        if (ObjectUtils.isNull(entity)) {
            // 读取数据库
            entity = this.getById(menuId);
            // 存入 redis
            this.updateRedisData(entity);
        }
        return entity;
    }

    @Override
    public RbacMenuEntity getMenuByCode(String code) {
        Long tenantId = SysAdminUtils.getCurrentTenantId();
        RbacMenuEntity entity = this.getRedisData(
                RbacAuthConstants.MENU_CODE_ID_KEY
                        .concat(String.valueOf(tenantId)), code);
        if (ObjectUtils.isNull(entity)) {
            QueryWrapper<RbacMenuEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("menu_code", code);
            entity = this.getOne(queryWrapper, false);
            if (ObjectUtils.notNull(entity)) {
                this.updateRedisData(entity);
            }
        }
        return entity;
    }

    @Override
    public void updateRedisData(RbacMenuEntity entity) {
        // menu id -> entity
        redisTemplate.opsForHash().put(
                RbacAuthConstants.MENU_ID_KEY, entity.getId(), entity);
        // menu code -> menu id
        redisTemplate.opsForHash().put(
                RbacAuthConstants.MENU_CODE_ID_KEY
                        .concat(String.valueOf(entity.getTenantId())),
                entity.getMenuCode(), entity.getId());
    }

    @Override
    public RbacMenuEntity getRedisData(String key, Object hashKey) {
        if (RbacAuthConstants.MENU_ID_KEY.equals(key)) {
            return (RbacMenuEntity) redisTemplate.opsForHash().get(
                    RbacAuthConstants.MENU_ID_KEY, hashKey);
        } else {
            Long menuId = (Long) redisTemplate.opsForHash().get(key, hashKey);
            if (ObjectUtils.notNull(menuId)) {
                return (RbacMenuEntity) redisTemplate.opsForHash().get(
                        RbacAuthConstants.MENU_ID_KEY, menuId);
            }
        }
        return null;
    }

    @Override
    public void deleteRedisData(RbacMenuEntity entity) {
        redisTemplate.opsForHash().delete(
                RbacAuthConstants.MENU_ID_KEY, entity.getId());
        redisTemplate.opsForHash().delete(
                RbacAuthConstants.MENU_CODE_ID_KEY
                        .concat(String.valueOf(entity.getTenantId())),
                entity.getMenuCode());
    }
}
