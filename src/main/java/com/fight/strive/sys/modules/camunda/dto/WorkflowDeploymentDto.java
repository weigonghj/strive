package com.fight.strive.sys.modules.camunda.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author ZHOUXIANG
 */
@Data
@Accessors(chain = true)
@ApiModel
public class WorkflowDeploymentDto {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("流程名称")
    private String name;

    @ApiModelProperty("部署时间")
    private Date deploymentTime;

    @ApiModelProperty("validatingSchema")
    private boolean validatingSchema;

    @ApiModelProperty("is new")
    private boolean isNew;

    @ApiModelProperty("source")
    private String source;

    @ApiModelProperty("租户ID")
    private String tenantId;
}
