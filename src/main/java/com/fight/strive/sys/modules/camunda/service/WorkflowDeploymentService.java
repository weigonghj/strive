package com.fight.strive.sys.modules.camunda.service;

import com.fight.strive.sys.modules.camunda.dto.WorkflowDeploymentDto;
import com.fight.strive.sys.modules.request.dto.PageRequest;
import com.fight.strive.sys.modules.response.dto.PageData;
import org.springframework.web.multipart.MultipartFile;

/**
 * 流程部署的管理
 *
 * @author ZHOUXIANG
 */
public interface WorkflowDeploymentService {

    /**
     * 部署流程文件
     *
     * @param file 文件
     * @param name 部署名称
     */
    void bpmnModelDeployFile(MultipartFile file, String name);

    /**
     * 部署流程文本
     *
     * @param text 文件内容
     * @param name 部署名称
     */
    void bpmnModelDeployText(String text, String name);

    /**
     * 重新部署流程定义
     *
     * @param id 部署ID
     */
    void redeployResource(String id);

    /**
     * 分页查询流程部署资源
     *
     * @param pageRequest 请求条件
     * @return 分页结果
     */
    PageData<WorkflowDeploymentDto> listDeploymentByPage(
            PageRequest<WorkflowDeploymentDto> pageRequest);

    /**
     * 删除流程部署
     *
     * @param id 部署ID
     */
    void deleteDeploymet(String id);
}
