package com.fight.strive.sys.modules.camunda.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fight.strive.sys.modules.camunda.dto.WorkflowDefinitionDto;
import com.fight.strive.sys.modules.camunda.dto.WorkflowQueryDto;
import com.fight.strive.sys.modules.camunda.entity.WorkflowDefinitionEntity;
import com.fight.strive.sys.modules.request.dto.PageRequest;
import com.fight.strive.sys.modules.response.dto.PageData;

/**
 * @author ZHOUXIANG
 */
public interface WorkflowDefinitionService
        extends IService<WorkflowDefinitionEntity> {

    /**
     * 分页查询最新版本流程定义
     *
     * @param pageRequest 分页请求
     * @return 分页结果
     */
    PageData<WorkflowDefinitionDto> listDefinitionByPage(
            PageRequest<WorkflowDefinitionDto> pageRequest);

    /**
     * 分页查询最新版本流程定义，以候选条件查询，
     * 自定义查询，未使用 camunda api
     *
     * @param pageRequest 分页请求
     * @return 分页结果
     */
    PageData<WorkflowDefinitionEntity> listProcessByPage(
            PageRequest<WorkflowQueryDto> pageRequest);

    /**
     * 删除流程定义
     *
     * @param dto 流程定义
     */
    void deleteProcessDefinition(WorkflowDefinitionDto dto);

    /**
     * 根据流程定义下载资源
     *
     * @param dto 参数
     */
    void downloadResourceFile(WorkflowDefinitionDto dto);

    /**
     * 获取 bpmn.xml String
     *
     * @param id process definition id
     * @return xml string
     */
    String getDefinitionXmlString(String id);
}
