package com.fight.strive.sys.modules.redis.subscribe;

import com.fight.strive.sys.modules.web.component.SpringContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.lang.NonNull;

/**
 * @author ZHOUXIANG
 */
@Slf4j
public class RedisSubscribe implements MessageListener {

    @Override
    public void onMessage(@NonNull Message message, byte[] pattern) {
        RedisSerializer<?> redisSerializer =
                SpringContextHolder.getBean(RedisSerializer.class);
        // 实际使用中，可以通过 redis 的反序列化得到指定对象
        Object msg = redisSerializer.deserialize(message.getBody());
        log.debug("redis 订阅收到的消息：{}", msg);
    }
}
