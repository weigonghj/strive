package com.fight.strive.sys.modules.rbac.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fight.strive.sys.modules.rbac.entity.RbacUserRoleEntity;

/**
 * @author ZHOUXIANG
 */
public interface RbacUserRoleMapper extends BaseMapper<RbacUserRoleEntity> {
}
