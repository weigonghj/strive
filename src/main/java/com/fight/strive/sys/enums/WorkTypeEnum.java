package com.fight.strive.sys.enums;

/**
 * 职位类型
 * @author ZHOUXIANG
 */
public enum WorkTypeEnum {

    /**
     * 全职
     */
    FULL_TIME,

    /**
     * 兼职或代理
     */
    PART_TIME
}
