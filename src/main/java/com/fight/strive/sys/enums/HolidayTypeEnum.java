package com.fight.strive.sys.enums;

/**
 * @author ZHOUXIANG
 */
public enum HolidayTypeEnum {


    /**
     * 放假
     */
    HOLIDAY,

    /**
     * 调班
     */
    WORKDAY
}
