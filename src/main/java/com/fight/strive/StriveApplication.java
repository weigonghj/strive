package com.fight.strive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author ZHOUXIANG
 */
@SpringBootApplication
@EnableFeignClients
public class StriveApplication {

    public static void main(String[] args) {
        SpringApplication.run(StriveApplication.class, args);
    }
}
