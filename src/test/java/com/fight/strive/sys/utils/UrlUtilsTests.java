package com.fight.strive.sys.utils;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @author ZHOUXIANG
 */
@Slf4j
public class UrlUtilsTests {

    @Test
    public void test() {
        String url = UrlUtils
                .joinUrl("http://www.xxx.com/rdsp","push");
        log.info("{}", url);
    }
}
