package com.fight.strive.sys.utils;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class StringUtilsTest {

    @Test
    public void testRemoveAllWhitespace() {
        String str = "abc, def\n, dddm\n";
        log.info("{}", StringUtils.removeAllWhitespace(str));
    }
}
