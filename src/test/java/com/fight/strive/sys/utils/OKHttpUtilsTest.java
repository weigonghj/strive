package com.fight.strive.sys.utils;

import lombok.extern.slf4j.Slf4j;
import okhttp3.FormBody;
import okhttp3.Request;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class OKHttpUtilsTest {

    @Test
    public void testGet() {
        Map<String, Object> params = new HashMap<>();
        params.put("scope", "provider");
        params.put("grant_type", "client_credentials");
        FormBody.Builder builder = new FormBody.Builder();
        params.forEach((key, value) -> builder.add(key, String.valueOf(value)));
        Request request = new Request.Builder()
                .addHeader("x-requested-with", "XMLHttpRequest")
                .addHeader("Authorization", "basic NjE2MGFmOTEzODI4NzBjYjo5ZmQyOGI1MTYxNjBhZjkxMzgyODcwY2I1MDA1NWRiZg==")
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .url("https://rd.shenhuan-tech.com/bsp-rd/oauth/token").post(builder.build()).build();
        String body = OKHttpUtils.call(request);
        log.info("{}", body);
    }
}
