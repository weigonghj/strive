package com.fight.strive.sys.utils;

import com.fight.strive.sys.modules.exception.ExceptionUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class ExceptionUtilsTests {

    @Test
    public void testGetStack() {
        try {
            Integer a = 12 / 2;
        } catch (Exception e) {
            String stack = ExceptionUtils
                    .getPrintStackTrace(e);
            log.info(stack);
        }
    }

    @Test
    public void testHandleStack() {
        String str = ExceptionUtils
                .formatStackTrace("abc\r\nddddd\r\n\r\nadfasdfa\r\nadf\r\n");
        log.info(str);
    }
}