package com.fight.strive.sys.utils;

import com.fight.strive.sys.enums.OperateStatusEnum;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class EnumTests {

    @Test
    public void testOperateStatusEnum() {
        log.info("{}", OperateStatusEnum.isY("N"));
        log.info("{}", OperateStatusEnum.isN("N"));
        log.info("{}", OperateStatusEnum.isR("R"));
    }
}
