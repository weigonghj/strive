package com.fight.strive.sys.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fight.strive.StriveApplicationTests;
import com.fight.strive.sys.utils.dto.Person;
import com.fight.strive.sys.modules.response.dto.ResponseStatusDto;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class JSONUtilsTests extends StriveApplicationTests {

    @Test
    public void testObj2Json() {
        Person person = new Person();
        person.setName("test");
        person.setAge(23);
        log.info(JSONUtils.obj2Json(person));
        log.info(JSONUtils.obj2Json(person, JsonInclude.Include.ALWAYS));
        log.info(JSONUtils.obj2Json(person, JsonInclude.Include.NON_NULL));
    }

    @Test
    public void testJson2Obj() {
        Person person = JSONUtils.json2Obj("{\"name\":\"cyan\",\"age\":2, \"sex\":\"male\"}", Person.class);
        log.info("{}", person);
    }

    @Test
    public void testEnumJson() {
        ResponseStatusDto<String> responseStatusDto = ResponseStatusDto.success();
        log.info(JSONUtils.obj2Json(responseStatusDto));
    }

    @Test
    public void testStringJson() {
        String string = "abcdefg";
        log.info(JSONUtils.obj2Json(string));
    }
}
