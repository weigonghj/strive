package com.fight.strive.sys.utils;

import com.fight.strive.sys.enums.TimeUnitEnum;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.Date;

@Slf4j
public class DateUtilsTests {

    @Test
    public void testGreaterThan() {
        Date start = DateUtils.parseDatetime("2020-06-20 00:00:00");
        Date end = DateUtils.parseDatetime("2020-06-30 00:00:00");
        log.info("{}", DateUtils.greaterThan(start, end, TimeUnitEnum.DAY));
    }
}
