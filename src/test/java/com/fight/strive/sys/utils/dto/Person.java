package com.fight.strive.sys.utils.dto;

import lombok.Data;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * 仅供测试Utils使用的模型类 Person
 */
@Data
@Root
public class Person {

    @Attribute
    private String name;

    @Element
    private int age;

    @Element(required = false)
    private String sex;

}
