package com.fight.strive.sys.utils;

import com.fight.strive.StriveApplicationTests;
import com.fight.strive.sys.utils.dto.Person;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class XMLUtilsTests extends StriveApplicationTests {

    @Test
    public void testObj2Xml() {
        Person person = new Person();
        person.setName("test");
        person.setAge(23);
        person.setSex("");
        log.info(XMLUtils.obj2Xml(person));
    }

    @Test
    public void testXml2Obj() {
        Person person = XMLUtils.xml2Obj("<person name=\"test\"><age>23</age><sex></sex></person>", Person.class);
        log.info("{}", person);
    }
}
