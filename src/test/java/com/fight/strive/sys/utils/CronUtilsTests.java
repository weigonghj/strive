package com.fight.strive.sys.utils;

import com.cronutils.model.Cron;
import com.cronutils.model.CronType;
import com.cronutils.model.definition.CronDefinition;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.model.time.ExecutionTime;
import com.cronutils.parser.CronParser;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.Date;

@Slf4j
public class CronUtilsTests {

    @Test
    public void test() {
        CronDefinition cronDefinition = CronDefinitionBuilder
                .instanceDefinitionFor(CronType.SPRING);
        CronParser cronParser = new CronParser(cronDefinition);
        Cron cron = cronParser.parse("0 0 1 1/1 * ?");
        cron.validate();
        ExecutionTime executionTime = ExecutionTime.forCron(cron);
        ZonedDateTime zonedDateTime = executionTime
                .lastExecution(ZonedDateTime.now())
                .orElse(null);
        assert zonedDateTime != null;
        Date date = Date.from(zonedDateTime.toInstant());
        log.info("{}", DateUtils.formatDatetime(date));
    }
}
