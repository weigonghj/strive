package com.fight.strive.sys.modules.oauth2;

import com.fight.strive.StriveApplicationTests;
import com.fight.strive.sys.modules.oauth2.entity.ClientDetailsEntity;
import com.fight.strive.sys.modules.oauth2.service.StriveClientDetailsService;
import com.fight.strive.sys.utils.DigestUtils;
import com.fight.strive.sys.utils.JSONUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import javax.annotation.Resource;

@Slf4j
public class StriveClientDetailsServiceTests extends StriveApplicationTests {

    @Resource
    private StriveClientDetailsService striveClientDetailsService;

    @Test
    public void testSaveClientDetails() {
        ClientDetailsEntity client = new ClientDetailsEntity();
        client.setClientId("client_id_123456");
        client.setClientSecret(DigestUtils.md5Hex("client_secret_123456"));
        client.setScopes("request");
        client.setAuthorizedGrantTypes("client_credentials,password");
        striveClientDetailsService.saveClientDetails(client);
    }

    @Test
    public void testGetClientDetails() {
        ClientDetailsEntity client = striveClientDetailsService
                .getClientDetailsByClientId("client_id_123456");
        log.info(JSONUtils.obj2Json(client));
    }
}
