package com.fight.strive.sys.modules.lock.service;

import com.fight.strive.sys.modules.lock.dto.Person;

@SuppressWarnings("unused")
public interface PersonService {

    void printPersonForRedisLock(Person person);

    void printPersonForLock4j(Person person);

}
