package com.fight.strive.sys.modules.file;

import com.fight.strive.StriveApplicationTests;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.StandardCharsets;

/**
 * 文件上传与下载测试类
 */
@Slf4j
public class FileControllerTests extends StriveApplicationTests {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    /**
     * 测试文件上传
     */
    @Test
    public void testUploadFile() throws Exception {
        String result = mockMvc.perform(
                MockMvcRequestBuilders
                        .multipart("/file/upload")
                        .file(
                                new MockMultipartFile("file", "test.txt",
                                        "multipart/form-date",
                                        "hello world".getBytes(StandardCharsets.UTF_8))
                        )
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString();
        System.out.println(result);
        //成功返回{"status":"success","data":"0053045b5802492e866b108d2ae46ee8.txt"}
        //意味着在C;\\Windows\\Temp目录下有一个0053045b5802492e866b108d2ae46ee8.txt文件，里面内容为"Hello World"
        //下面在会用这个文件作下载测试，注意，再次测试时，文件名称是不相同的
        //如果失败会返回{"status":"error","message":"文件上传异常"}
    }

    /**
     * 测试图片上传
     */
    @Test
    public void testUploadImage() throws Exception {
        String result = mockMvc.perform(
                MockMvcRequestBuilders
                        .multipart("/file/image")
                        .file(
                                new MockMultipartFile("image", "test.jpg",
                                        "multipart/form-date",
                                        "hello world".getBytes(StandardCharsets.UTF_8))
                        )
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString();
        log.info(result);
        //成功返回{"status":"success","data":"data:image/jpg;base64,aGVsbG8gd29ybGQ="}，
        // 这里的data其实是"Hello World"字节的Base64编码，图片的道理类似。
        //如果失败会返回{"status":"error","message":"图片上传异常"}
    }

    /**
     * 测试文件下载
     */
    @Test
    public void testDownloadFile() throws Exception {
        String result = mockMvc.perform(
                MockMvcRequestBuilders
                        .get("/file/download")
                        .param("fileName", "0053045b5802492e866b108d2ae46ee8.txt")
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString();
        log.info(result);
        //返回的是hello world，这是文件中的内容，这里直接将下载文件的流转成字符串输出。
        //注意：文件下载Controller不要返回任务内容，如果返回，则返回的内容会追加的response中之前文件流的后面，
        // 导致文件内容不正确。
    }

}
