package com.fight.strive.sys.modules.mail;

import com.fight.strive.StriveApplicationTests;
import com.fight.strive.sys.modules.mail.exception.SendMailException;
import com.fight.strive.sys.modules.mail.service.MailService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * 框架邮件发送服务测试类
 */
public class MailServiceTests extends StriveApplicationTests {

    @Autowired
    private MailService mailService;

    /**
     * 测试简单的文本发送，验证时注意垃圾邮件
     */
    @Test
    public void testSendSimpleMail() {
        mailService.sendSimpleMail("来自strive框架开发者的问候", "欢迎使用strive开发框架", "352016780@qq.com");
    }

    /**
     * 测试邮件附件发送，验证时注意垃圾邮件
     *
     * @throws SendMailException
     */
    @Test
    public void testSendAttachmentMail() throws SendMailException, IOException {
        final File file = File.createTempFile("attachment", ".txt");
        List<File> files = new ArrayList<>();
        files.add(file);
        mailService.sendAttachmentMail("来自strive框架开发者的问候", "欢迎使用strive开发框架", files, "352016780@qq.com");
        file.deleteOnExit();
    }

    /**
     * 测试Html邮件发送，验证时注意垃圾邮件
     *
     * @throws SendMailException
     */
    @Test
    public void testSendHtmlMail() throws SendMailException {
        mailService.sendHtmlMail("来自strive框架开发者的问候", "<h3>欢迎使用strive开发框架</h3>", "352016780@qq.com");
    }
}
