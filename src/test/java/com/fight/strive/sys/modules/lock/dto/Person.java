package com.fight.strive.sys.modules.lock.dto;

import lombok.Data;

/**
 * 分布式锁单元测试数据模型Person类
 */
@Data
public class Person {

    private String name;

    private String sex;

    private int age;
}
