package com.fight.strive.sys.modules.lock;

import com.fight.strive.StriveApplicationTests;
import com.fight.strive.sys.modules.lock.dto.Person;
import com.fight.strive.sys.modules.lock.service.PersonService;
import com.fight.strive.sys.utils.TimeUtils;
import org.junit.Test;

import javax.annotation.Resource;

public class LockTests extends StriveApplicationTests {

    @Resource
    private PersonService personService;

    @Test
    public void testStriveLock() {
        Person person = new Person();
        person.setName("test");
        person.setSex("male");
        person.setAge(24);
        personService.printPersonForRedisLock(person);
        personService.printPersonForRedisLock(person);
        TimeUtils.delay(30000);
    }

    @Test
    public void testLock4j() {
        Person person = new Person();
        person.setName("test");
        person.setSex("male");
        person.setAge(24);
        personService.printPersonForLock4j(person);
        personService.printPersonForLock4j(person);
        TimeUtils.delay(30000);
    }
}
