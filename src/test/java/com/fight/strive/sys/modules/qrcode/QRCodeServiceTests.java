package com.fight.strive.sys.modules.qrcode;


import com.fight.strive.StriveApplicationTests;
import com.fight.strive.sys.modules.qrcode.service.impl.QrCodeServiceImpl;
import com.fight.strive.sys.utils.ImageUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * 二维码生成与解析服务类测试
 */
public class QRCodeServiceTests extends StriveApplicationTests {

    @Autowired
    private QrCodeServiceImpl qrCodeService;

    /**
     * 生成二维码图片的测试
     */
    @Test
    public void testCreateQRCode() throws Exception {
        //生成一张600x600r的二维码图片
        byte[] codeImage = qrCodeService.createQrCode("strive framework", 600);
        //ImageUtils.byte2image(codeImage, "D:/qrcode.jpg");
        ImageUtils.byte2image(codeImage, new FileOutputStream("D:/qrcode.jpg"));
    }

    /**
     * 解析二维码图片流的测试
     */
    @Test
    public void testDecodeImage() throws FileNotFoundException {
        //String codeStr = qrCodeService.decodeImage(new File("D:/qrcode.jpg"));
        String codeStr = qrCodeService.decodeImage(new FileInputStream("D:/qrcode.jpg"));
        System.out.println(codeStr);
        //打印的字符串是strive framework
    }
}
