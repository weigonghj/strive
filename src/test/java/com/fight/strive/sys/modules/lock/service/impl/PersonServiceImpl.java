package com.fight.strive.sys.modules.lock.service.impl;

import com.baomidou.lock.annotation.Lock4j;
import com.fight.strive.sys.modules.lock.annotation.StriveLock;
import com.fight.strive.sys.modules.lock.dto.Person;
import com.fight.strive.sys.modules.lock.service.PersonService;
import com.fight.strive.sys.utils.JSONUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PersonServiceImpl implements PersonService {

    @Override
    @Async
    @StriveLock(key = "#person.name", block = true)
    public void printPersonForRedisLock(Person person) {
        try {
            Thread.sleep(10000);
            log.info(JSONUtils.obj2Json(person));
        } catch (Exception e) {
            log.error("单元测试printPerson错误", e);
        }
    }

    @Override
    @Async
    // 尝试tryTimeout毫秒后，返回
    @Lock4j(keys = "#person.name", tryTimeout = 3000)
    public void printPersonForLock4j(Person person) {
        try {
            Thread.sleep(5000);
            log.info(JSONUtils.obj2Json(person));
        } catch (Exception e) {
            log.error("单元测试printPerson错误", e);
        }
    }

}
