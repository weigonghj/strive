package com.fight.strive.sys.modules.scheduling;

import com.fight.strive.StriveApplicationTests;
import com.fight.strive.sys.modules.scheduling.job.IStriveJob;
import com.fight.strive.sys.modules.scheduling.service.HessianService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.MalformedURLException;

@Slf4j
public class HessianServiceTests extends StriveApplicationTests {

    @Autowired
    private HessianService hessianService;

    @Test
    public void testHessian() throws MalformedURLException {
        IStriveJob job = hessianService.getStriveJob("http://localhost:8080/strive/striveTestJob.job");
        job.handler(null);
        log.info("完成");
    }
}
