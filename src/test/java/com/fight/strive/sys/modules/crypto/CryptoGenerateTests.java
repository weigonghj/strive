package com.fight.strive.sys.modules.crypto;

import com.baomidou.dynamic.datasource.toolkit.CryptoUtils;
import com.fight.strive.StriveApplicationTests;
import org.jasypt.encryption.StringEncryptor;
import org.junit.Test;

import javax.annotation.Resource;

public class CryptoGenerateTests extends StriveApplicationTests {

    @Resource
    private StringEncryptor stringEncryptor;

    /**
     * 动态数据源加密工具类
     */
    @Test
    public void testGetCrypto() throws Exception {
        String origin = "123456";
        String encode = CryptoUtils.encrypt(origin);
        String decode = CryptoUtils.decrypt(encode);
        System.out.println(encode);
        System.out.println(decode);
    }

    /**
     * 其他密码加密类
     */
    @Test
    public void testGetCrypto2() {
        String origin = "123456";
        String encode = stringEncryptor.encrypt(origin);
        System.out.println(encode);
        String decode = stringEncryptor.decrypt(encode);
        System.out.println(decode);
    }
}
