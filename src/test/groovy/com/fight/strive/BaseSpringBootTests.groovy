package com.fight.strive

import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

/**
 * groovy测试基类
 */
@Rollback
@SpringBootTest
@ContextConfiguration(classes = StriveApplication.class)
class BaseSpringBootTests extends Specification {
}
