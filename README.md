## strive框架介绍

> 使用前请仔细阅读本文档

基于 SpringBoot 2.x 的 JavaEE 基础开发框架

本框架是基于SpringBoot开源技术，结合平时工作开发的经验，为了简化开发工作，提高开发效率，
自主研究集成的框架。

本框架细化了SpringBoot2.x在开发应用中的配置，并在本文档中介绍各种配置作用及修改所表示
的不同功能。

本框架采用完全开源的方式，框架中的配置与扩展的源码都是可以进行二次开发，同时具有学习研究参考的作用。

## strive 框架的构成

本框架主要由SpringBoot2.x（Spring5，Spring MVC等技术）、数据库访问（Hibernate、Mybatis Plus）、
安全框架（Spring Security）、页面模板（thymeleaf、freemarker）。

* Hibernate主要用来创建数据库实体，以实现自动建表的功能，包括简单的增删改查业务处理。
* Mybatis主要用于复杂的结果集查询，使用SQL注解与Provider方式处理较复杂的增删改查业务。
* Spring Security主要用于安全拦截与处理，自定义过滤器、验证处理器，包括同步与异步登录方式。
* 本框架使用Thymeleaf（类似JSP页面）作为前端页面的显示模板，其它框架需要用户自己修改配置，本框架暂不研究针对别的模板的集成。
* 本框架采用Java Config代码与注解的配置方式，摈弃xml配置方式，使用此框架的开发者须接受此约定。

框架中集成的相关技术如下所示：

* SpringMVC框架
* SpringIoc容器
* Hibernate数据库访问
* MyBatis plus数据库访问
* 多个数据源灵活切换配置
* 阿里Druid数据库连接池,多数据源插件
* SpringSecurity/oauth2安全框架
* thymeleaf/freemarker页面模板
* json与xml处理技术
* 基于Oauth2.0单点登录功能
* mail邮件发送功能
* 异步执行功能
* 二维码生成服务
* 基于Redis缓存
* 基于Redis实现的分布式锁
* 使用注解行为日志
* 自动记录访问日志
* 自动记录错误日志
* 定时任务调度服务与管理
* 集成camunda工作流引擎
* 基于websocket移动端扫码登录PC端

## 从gitee（码云）上获取该项目

- 使用git命令获取项目：`git clone https://gitee.com/zxstrive/strive.git`。
- 本框架使用gradle构建，关于gradle的安装，参考互联网。
- 使用gradle命令生成eclipse项目：`gradle eclipse`。需安装gradle插件。
- Intellij Idea可以直接打开工程。关闭Inceptions设置。
- 需要安装lombok工具插件（使用注解生成getter与setter方法），否则会报错。
- 框架使用内置H2数据库，构建好后可以直接运行，不需要任何其它操作，如果要使用其它数据库可修改c3p0.properties。
- 框架黙认超级用户superadmin/123456，可使用此用户名与密码登录访问权限控制页面。
- 需使用jdk1.8.0_144，其他版本可能报错无法启动。
- 及时修改idea编码为UTF-8。
- 第一时间将settings.gradle中的rootName修改为自己的项目名称。
- 删除项目根目录下的.git并重新初始化。
- 修改.idea/workspace.xml文件，在PropertiesComponent下添加：\<property name="dynamic.classpath" value="true" /\>

## strive 框架的目录使用约定

源代码目录：
- com.fight.strive
    - app //业务开发时使用的目录 
        - enums // 业务开发通用枚举型常量包
        - modules //此包下存放与业务模块相关的子包，模块下包含如controller、service、dao(mybatis)、dto、entity、repo(jpa)等相关子包
        - property // 应用相关的属性配置
    - sys //框架相关的配置与工具或扩展类，正常开发时不用修改此目录下的代码，尽量不要使用此目录，框架代码可根据需要修改
        - enums // 系统框架通用枚举型常量包
        - utils //框架预定义的工具类存放目录
        - modules //此包下存放系统模块子包
        
资源目录：
- static //静态资源存放的文件
    - app //开发中使用的css、js与静态html文件存放目录
    - sys //框架预定义的资源文件存放目录，开发中尽量不要向此类目录中添加内容
- templates.views //动态模板存放的文件
    - admin //需要访问权限认证的页面放在此目录中
        - modules //根据业务模块进行区分的页面
    - user //不需要认证的页面放在此目录中
        - modules //根据业务模块进行区分的页面

资源文件：
- strive.properties //strive框架属性文件，可以通过StriveProperties类进行注入使用；在application.yml中使用strive.开头的配置。
- banner.txt // console banner 图案文字
- application.yml //Spring Boot 核心配置文件，active=dev,pro
- application-dev.yml // 开发配置
- application-pro.yml // 运行配置

> 注意：开发中使用的源代码目录为com.fight.strive.app，使用的资源目录为static/app和
templates.views。其它目录尽量不要修改。

#### strive-web，vue应用，为用户系统管理平台的前端应用，地址：https://gitee.com/zxstrive/strive-web.git
#### strive-app，uni-app，为用户系统管理台的移动端应用，地址：https://gitee.com/zxstrive/strive-app